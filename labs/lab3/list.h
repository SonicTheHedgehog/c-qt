using namespace std;
#include <string>
#pragma once
#include "dynarray.h"
template <typename T>

class List
{
    DynamicArray<T> array_;
    size_t size_;

public:
    List()
    {
        size_ = 0;
    }
    ~List() {}
    size_t size()
    {
        return size_;
    }
    T &at(int index)
    {
        if (index <= size_)
        {
            return array_.at(index);
        }
        else
        {
            cerr << "wrong index" << endl;
        }
    }
    void push_back(T &value)
    {
        if (size_ == array_.size())
        {
            array_.resize();
        }
        array_.at(size_) = value;
        size_++;
    }
    int index_of(T &value)
    {
        int index = -1;
        for (int i = 0; i < size_; i++)
        {
            if (array_.at(i) == value)
            {
                return i;
            }
        }
        return index;
    }
    void remove(T &value)
    {
        int index = index_of(value);
        if (index != -1)
        {
            for (int i = index; i < size_ - 1; i++)
            {
                array_.at(i) = array_.at(i + 1);
            }
            size_--;
            index = index_of(value);
        }
    }
    void print()
    {
        for (int i = 0; i < size_; i++)
        {
            cout << "name " << array_.at(i).name << "\tusers " << array_.at(i).users << "\trevenue " << array_.at(i).revenue << "\tactive " << array_.at(i).active << endl;
        }
    }

    void clear()
    {
        return size_ = 0;
    }
};