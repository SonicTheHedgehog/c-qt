#pragma once
#include "list.h"
#include "csv.h"
string read_file(const string &filename);
int write_file(const string &filename, string &line);
