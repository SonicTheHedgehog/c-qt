#include "options.h"
#include <iostream>
#include <sstream>
using namespace std;

Options getProgramOptions(int argc, char *argv[])
{
    if (argc > 8 || argc < 2)
    {
        cerr << "Error input" << endl;
        exit(1);
    }
    else
    {
        Options O;
        string a, b;
        bool infileexists = false;
        for (int i = 1; i < argc; i++)
        {
            b = string(argv[i]);
            if (b.substr(0, 3) == "in.")
            {
                O.input_file_name = string(argv[i]).substr(string(argv[i]).find(string("in.")) + 3);
                infileexists = true;
            }
            if (string(argv[i]) == "-b")
            {
                O.build_bstree = true;
            }
        }
        if (!infileexists)
        {
            cerr << "There is no infile." << endl;
            exit(1);
        }
        for (int i = 1; i < argc - 1; i++)
        {
            a = string(argv[i]);
            if (a == "-n")
            {
                char *p = argv[i + 1];
                bool isd = true;
                if (!isdigit(*p))
                {
                    isd = false;
                    break;
                }
                if (isd)
                    O.n_process = atof(argv[i + 1]);
            }
            if (a == "-o" && string(argv[i + 1]).substr(0, 4) == "out.")
            {
                O.output_file_name = string(argv[i + 1]).substr(string(argv[i + 1]).find(string("out.")) + 4);
            }
        }
        return O;
    }
}