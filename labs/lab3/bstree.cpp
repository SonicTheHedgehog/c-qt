#include "bstree.h"
#include <cassert>
#include <iostream>
size_t BSTree::size()
{
    return size_;
}
int getKey(Social &value)
{
    return value.id;
}

static void insertNode(BinTree *node, BinTree *newNode);
void BSTree::insert(Social &value)
{
    int key = getKey(value);
    BinTree *newNode = new BinTree{key, value};
    this->size_ += 1;
    if (this->root_ == nullptr)
    {
        this->root_ = newNode;
    }
    else
    {
        insertNode(this->root_, newNode);
    }
}
static void insertNode(BinTree *node, BinTree *newNode)
{
    assert(node != nullptr);
    if (newNode->item.id == node->item.id)
    {
        fprintf(stderr, "Key %i already exists in BST\n", newNode->id);
        abort();
    }
    else if (newNode->item.id < node->item.id)
    {
        if (node->left == nullptr)
        {
            node->left = newNode;
        }
        else
        {
            insertNode(node->left, newNode);
        }
    }
    else
    {
        if (node->right == nullptr)
        {
            node->right = newNode;
        }
        else
        {
            insertNode(node->right, newNode);
        }
    }
}

static void printValueOnLevel(BinTree *node, char pos, int depth)
{
    for (int i = 0; i < depth; i++)
    {
        cout << "....";
    }
    cout << pos;

    if (node == nullptr)
    {
        cout << " - " << endl;
    }
    else
    {
        cout << " id=" << node->item.id << " name " << node->item.name << " users " << node->item.users << " revenue " << node->item.revenue << " active users " << node->item.active << endl;
    }
}

static void printNode(BinTree *node, char pos, int depth)
{
    bool hasChild = node != nullptr && (node->left != nullptr || node->right != nullptr);
    printValueOnLevel(node, pos, depth);
    if (hasChild)
        printNode(node->left, 'L', depth + 1);
    if (hasChild)
        printNode(node->right, 'R', depth + 1);
}
static void printBinTree(BinTree *root)
{
    printNode(root, '+', 0);
}
void BSTree::printTree()
{
    printBinTree(this->root_);
}
static void modifyTreeOnRemove(BinTree *node, BinTree *parent);

static Social removeNode(BinTree *node, int key, BinTree *parent)
{
    if (node == nullptr)
    {
        fprintf(stderr, "Key `%i` not found on removal\n", key);
        abort();
    }
    if (key < node->id)
        return removeNode(node->left, key, node);
    else if (key > node->id)
        return removeNode(node->right, key, node);
    else
    {
        modifyTreeOnRemove(node, parent);
        Social removedValue = node->item;
        delete node;
        return removedValue;
    }
}
static BinTree *getRemoveReplacementNode(BinTree *node);

static void modifyTreeOnRemove(BinTree *node, BinTree *parent)
{
    BinTree *replacementNode = getRemoveReplacementNode(node);
    if (parent->left == node)
        parent->left = replacementNode;
    else
        parent->right = replacementNode;
}

static BinTree *modifyTreeOnCaseC(BinTree *node);

static BinTree *getRemoveReplacementNode(BinTree *node)
{
    if (node->left == nullptr && node->right == nullptr)
    {
        return nullptr;
    }
    else if (node->left == nullptr || node->right == nullptr)
    {
        BinTree *child = (node->left != nullptr) ? node->left : node->right;
        return child;
    }
    else
    {
        return modifyTreeOnCaseC(node);
    }
}
static BinTree *searchMin(BinTree *node);

static BinTree *modifyTreeOnCaseC(BinTree *node)
{
    BinTree *minNode = searchMin(node->right);
    int minKey = minNode->id;
    Social removedValue = removeNode(node->right, minKey, node);
    BinTree *newMin = new BinTree{minKey, removedValue};
    newMin->left = node->left;
    newMin->right = node->right;
    return newMin;
}

static BinTree *searchMin(BinTree *node)
{
    if (node == nullptr)
        return nullptr;
    if (node->left == nullptr)
        return node;
    return searchMin(node->left);
}

Social BSTree::remove(int id)
{
    Social S;
    BinTree fakeParent{-1, S};
    fakeParent.left = this->root_;
     S = removeNode(this->root_, id, &fakeParent);
    this->root_ = fakeParent.left;
    this->size_--;
    return S;
}void BSTree::clear()
{
    while (size_ != 0)
    {
        Social tmp = remove(root_->id);
    }
}
