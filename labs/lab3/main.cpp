#include "list.h"
#include "fs.h"
#include "options.h"
#include "bstree.h"
List<Social> createSocialListFromTable(StringTable &csvTable);
StringTable createTableFromList(List<Social> &array);
void processremove(List<Social> &items, float n);
int main(int argc, char *argv[])
{
    Options O;
    O = getProgramOptions(argc, argv);
    string line = read_file(O.input_file_name);
    StringTable inCsvTable = Csv_parse(line);
    cout << "\tString Table" << '\n';
    inCsvTable.print();
    List<Social> list = createSocialListFromTable(inCsvTable);
    cout << "\tList" << '\n';
    list.print();
    BSTree tree;
    if (O.build_bstree == true)
    {
        for (int i = 0; i < list.size(); i++)
        {
            tree.insert(list.at(i));
        }
        cout << "\tTree" << endl;
        tree.printTree();
    }
    if (!isnanf(O.n_process))
    {
        int id[list.size()], length = 0;
        for (int i = 0; i < list.size(); i++)
        {
            if (list.at(i).users > O.n_process)
            {
                id[length] = list.at(i).id;
                length++;
            }
        }
        processremove(list, O.n_process);
        cout << "\tNew List" << '\n';
        list.print();

        if (O.build_bstree == true)
        {
            for (int i = 0; i < length; i++)
            {
                tree.remove(id[i]);
            }
            cout << "\tNew Tree" << '\n';
            tree.printTree();
            tree.clear();
        }
    }
    StringTable outCsvtable = createTableFromList(list);
    cout << "\tNew String Table" << '\n';
    outCsvtable.print();
    if (O.output_file_name != "")
    {
        string csvtable = Csv_toString(outCsvtable);
        write_file(O.output_file_name, csvtable);
    }
    return 0;
}
List<Social> createSocialListFromTable(StringTable &csvTable)
{
    List<Social> list;
    Social media;
    for (int i = 0; i < csvTable.size_rows(); i++)
    {
        media.name = csvTable.at(i, 0);

        media.users = stof(csvTable.at(i, 1));
        media.revenue = csvTable.at(i, 2);
        media.active = csvTable.at(i, 3);
        media.id = stoi(csvTable.at(i, 4));
        list.push_back(media);
    }
    return list;
}
StringTable createTableFromList(List<Social> &array)
{
    StringTable table(array.size(), 5);
    for (int i = 0; i < table.size_rows(); i++)
    {
        table.at(i, 0) = array.at(i).name;
        ostringstream os, ss;
        os << array.at(i).users;
        table.at(i, 1) = os.str();
        table.at(i, 2) = array.at(i).revenue;
        table.at(i, 3) = array.at(i).active;
        ss << array.at(i).id;
        table.at(i, 4) = ss.str();
    }
    return table;
}
void processremove(List<Social> &items, float n)
{
    for (int i = 0; i < items.size(); i++)
    {
        if (items.at(i).users > n)
        {
            items.remove(items.at(i));
            i--;
        }
    }
}