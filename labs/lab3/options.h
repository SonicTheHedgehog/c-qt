#pragma once
#include <math.h>
#include <string>
using namespace std;
struct Options
{
    string input_file_name;
    float n_process=NAN;
    string output_file_name;
    bool build_bstree;
};

Options getProgramOptions(int argc, char *argv[]);