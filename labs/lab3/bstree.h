#include "bintree.h"
class BSTree
{
    BinTree *root_ = nullptr;
    size_t size_ = 0;

public:
    size_t size();
    void insert(Social& value);
    Social remove(int id);
    void clear();
    void printTree();
};

int getKey(Social &value);