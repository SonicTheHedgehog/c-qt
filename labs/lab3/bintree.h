typedef struct _BinTree BinTree;
#include "social.h"
struct _BinTree
{
	int id;
	Social item;
	BinTree *left;
	BinTree *right;
	_BinTree(int k, Social &v):id{k},item{v},left{NULL}, right{NULL} {}
};

