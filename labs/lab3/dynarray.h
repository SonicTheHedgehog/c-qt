#pragma once
#include "social.h"
#include <iostream>
using namespace std;

template <typename T>

class DynamicArray
{
    T *media;
    size_t _capacity;

public:
    DynamicArray()
    {
        _capacity = 16;
        media = new T[16];
    }
    ~DynamicArray()
    {
        delete[] media;
    }
    size_t size()
    {
        return _capacity;
    }

    T &at(int index)
    {
        if (index >= _capacity)
        {
            std::cerr << "wrong index" << std::endl;
        }
        else
        {
            return media[index+1];
        }
    }
    void resize()
    {
        size_t newSize = _capacity * 2;
        T *newmedia = new T[newSize];
        for (int i = 0; i < _capacity; i++)
        {
            newmedia[i] = media[i];
        }
        delete[] media;
        media = newmedia;
        _capacity = newSize;
    };
};