#ifndef LOGIN_H
#define LOGIN_H
#include "sqlite_storage.h"
#include <QAction>
#include <QMessageBox>
#include <QDialog>

namespace Ui {
class login;
}

class login : public QDialog
{
    Q_OBJECT

public:
    explicit login(QWidget *parent = 0, Storage *storage=nullptr);
    ~login();
    int getUserId();

private slots:
    void on_oklog_clicked();

    void on_nolog_clicked();

private:
    Ui::login *ui;
    Storage *storage_;
    int user_id;
};

#endif // LOGIN_H
