#ifndef EDITSOCIAL_H
#define EDITSOCIAL_H
#include "sqlite_storage.h"
#include <QDialog>
#include <QAction>
#include <QMessageBox>
#include <QVariant>
#include <QVector>
#include <QListWidgetItem>
#include <QFileDialog>
namespace Ui {
class EditSocial;
}

class EditSocial : public QDialog
{
    Q_OBJECT

public:
    explicit EditSocial(QWidget *parent = 0, Storage * storage=nullptr);
    ~EditSocial();

    Social MainSMedia();
    void EditLabels(const Social &media);
private slots:
    void on_Addbut_clicked();

    void on_Removebut_clicked();

    void on_listWidget_itemClicked(QListWidgetItem *item);

    void on_listWidget_2_itemClicked(QListWidgetItem *item);

private:
    Ui::EditSocial *ui;
    Storage *storage_;
    int getGroupId(QString groupname);
    int social_id;
};

#endif // EDITSOCIAL_H
