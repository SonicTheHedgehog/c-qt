#include "editsocial.h"
#include "ui_editsocial.h"
#include <QListWidget>
EditSocial::EditSocial(QWidget *parent, Storage * storage) :
    QDialog(parent),
    ui(new Ui::EditSocial)
{
    ui->setupUi(this);
    this->storage_=storage;
    vector<Groups> gr=storage_->getAllGroups();
    ui->listWidget->clear();
    ui->Addbut->setEnabled(false);
    ui->Removebut->setEnabled(false);
    for(auto it=gr.begin(); it!=gr.end(); it++)
    {
        QListWidget * listWidget=ui->listWidget;
        QVariant var = QVariant::fromValue((*it));
        QListWidgetItem * new_item = new QListWidgetItem();
        new_item->setText(QString::fromStdString(it->name));
        new_item->setData(Qt::UserRole, var);
        listWidget->addItem(new_item);
    }
}

EditSocial::~EditSocial()
{
    delete ui;
}
Social EditSocial::MainSMedia()
{
    Social media;
    media.name= ui->lineEdit->text().toStdString();
    media.revenue= ui->dSpinBox3->value();
    media.active= ui->dSpinBox2->value();
    media.users= ui->dSpinBox->value();
    return media;
}

void EditSocial::EditLabels(const Social &media)
{
    ui->lineEdit->setText(QString::fromStdString(media.name));
    ui->dSpinBox->setValue(media.users);
    ui->dSpinBox3->setValue(media.revenue);
    ui->dSpinBox2->setValue(media.active);
    vector<Groups> gr2=storage_->getAllSocialGroups(media.id);
    social_id=media.id;
    ui->listWidget_2->clear();
    for(auto it=gr2.begin(); it!=gr2.end(); it++)
    {
        QListWidget * listWidget_2=ui->listWidget_2;
        QVariant var = QVariant::fromValue((*it));
        QListWidgetItem * new_item = new QListWidgetItem();
        new_item->setText(QString::fromStdString(it->name));
        new_item->setData(Qt::UserRole, var);
        listWidget_2->addItem(new_item);
    }
}
int EditSocial::getGroupId(QString groupname) {

    QSqlQuery query;
    query.prepare("SELECT * FROM groups WHERE name = :name");
    query.bindValue(":name", groupname);
    if (!query.exec())
    {
        qDebug() << "get group id error:" << query.lastError();
        return 0;
    }
    if (query.next())
    {
        int group_id = query.value("id").toInt();
        return group_id;
    }
    else
    {
        return 0;
    }
}
void EditSocial::on_Addbut_clicked()
{
    QList<QListWidgetItem *> items = ui->listWidget->selectedItems();
    if (items.count() ==1)
    {

        QListWidgetItem * selectedItem = items.at(0);
        QVariant var = selectedItem->data(Qt::UserRole);
        Groups tmp = var.value<Groups>();
        vector<Groups> gr2=storage_->getAllSocialGroups(social_id);
        int count=0;
        for(auto it=gr2.begin(); it!=gr2.end(); it++)
        {
            QVariant var2 = QVariant::fromValue((*it));
            Groups tmp2=var2.value<Groups>();
            if(tmp.id==tmp2.id)
            {
                QMessageBox::information(this, "Error", "This group is already exists in  this social media");
                count++;
            }
        }
        if(count==0)
        {
            int group_id=tmp.id;
            if (group_id != 0)
            {
                bool inserted = storage_->insertSocialGroups(social_id, group_id);
                if (!inserted)
                {
                    QMessageBox::information(this, "Smth went wrong", "Can't insert this group");
                }
                else
                {
                    experimental::optional<Groups> tmp = storage_->getGroupById(group_id);
                    if (tmp)
                    {
                        Groups gr = tmp.value();
                        QString inputText = QString::fromStdString(gr.name);
                        QListWidgetItem * new_item = new QListWidgetItem(inputText);
                        QVariant var = QVariant::fromValue(gr);
                        new_item->setData(Qt::UserRole, var);
                        ui->listWidget_2->addItem(new_item);
                    }
                }
            }
            else
            {
                QMessageBox::information(this, "Smth went wrong", "Can't insert this group");
            }
        }
    }
}

void EditSocial::on_Removebut_clicked()
{
    QList<QListWidgetItem *> items = ui->listWidget_2->selectedItems();
    if (items.count() > 0)
    {
        QMessageBox::StandardButton reply = QMessageBox::question(this, "On remove", "Do you really want to remove this group?");
        if (reply == QMessageBox::StandardButton::Yes)
        {
            bool removed=true;
            foreach (QListWidgetItem * item, items)
            {
                QVariant var = item->data(Qt::UserRole);
                Groups tmp = var.value<Groups>();
                removed=storage_->removeSocialGroups(social_id,tmp.id) ;
                delete ui->listWidget_2->takeItem(ui->listWidget_2->row(item));
            }
            if (removed)
            {
                QMessageBox::information(this, "Success", "Group has been removed");
            }
            else
            {
                QMessageBox::information(this, "Error", "Can't remove this group");
            }
        }
    }
}

void EditSocial::on_listWidget_itemClicked(QListWidgetItem *item)
{
    ui->Addbut->setEnabled(true);
}

void EditSocial::on_listWidget_2_itemClicked(QListWidgetItem *item)
{
    ui->Removebut->setEnabled(true);
}
