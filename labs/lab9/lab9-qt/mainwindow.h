#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include <QMainWindow>
#include <QDebug>
#include <QAction>
#include <QFileDialog>
#include <QListWidgetItem>
#include <QMessageBox>
#include <QMainWindow>
#include "dialog.h"
#include "sqlite_storage.h"
#include "login.h"
#include "editsocial.h"
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
private slots:
    void OpenStorage();
    void Logout();
    void Exit();
    void on_listWidget_itemClicked(QListWidgetItem *item);
    void on_Add_clicked();
    void on_Edit_clicked();
    void on_remove_clicked();

private:
    Ui::MainWindow *ui;
    Storage *storage_;
    int user_id;
    void visibility();
    void media_details(const Social & media);
};

#endif // MAINWINDOW_H
