#ifndef GRUPS_H
#define GRUPS_H
#pragma once
#include <string>
using namespace std;
struct Groups
{
    int id;
    string name;
    int users;
    string admin;
};

#endif // GRUPS_H
Q_DECLARE_METATYPE(Groups)
