#include "dialog.h"
#include "ui_dialog.h"

Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);
    setWindowTitle(tr("Social media"));
}

Dialog::~Dialog()
{
    delete ui;
}
 Social Dialog::MainSMedia()
 {
     Social media;
     media.name= ui->lineEdit->text().toStdString();
     media.revenue= ui->dSpinBox3->value();
     media.active= ui->dSpinBox2->value();
     media.users= ui->dSpinBox->value();
     return media;
 }

 void Dialog::EditLabels(const Social &media)
 {
     ui->lineEdit->setText(QString::fromStdString(media.name));
     ui->dSpinBox->setValue(media.users);
     ui->dSpinBox3->setValue(media.revenue);
     ui->dSpinBox2->setValue(media.active);
 }
