#ifndef SQLITE_STORAGE_H
#define SQLITE_STORAGE_H
#pragma once
#include <QtSql>
#include <QSqlDatabase>
#include "storage.h"
#include <experimental/optional>
using namespace std;
class SqliteStorage: public Storage
{
//protected:
        const string dir_name_;
        QSqlDatabase db_;
public:
        SqliteStorage(const string & dir_name);//: Storage(dir_name) {db_ = QSqlDatabase::addDatabase("QSQLITE");}

        bool isOpen() const;
        bool open();
        void close();


         vector<Social> getAllMedia(void) ;
         experimental::optional<Social> getMediaById(int id) ;
         bool updateMedia(const Social &media) ;
         bool removeMedia(int id) ;
         int insertMedia(const Social &media) ;

          vector<Groups> getAllGroups(void);
            experimental::optional<Groups> getGroupById(int id);
           bool updateGroup(const Groups &group);
           bool removeGroup(int id);
           int insertGroup(const Groups &group);
           // users
             experimental::optional<Users> getUserAuth( const string & username, const string & password);
              vector<Social> getAllUserSocial(int user_id);
              bool addUserid(int user_id, int gid);
              // links
             vector<Groups> getAllSocialGroups(int social_id);
              bool insertSocialGroups(int social_id, int group_id);
              bool removeSocialGroups(int social_id, int group_id);

};

#endif // SQLITE_STORAGE_H
