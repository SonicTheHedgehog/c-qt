#include "login.h"
#include "ui_login.h"

login::login(QWidget *parent, Storage *storage) :
    QDialog(parent),
    ui(new Ui::login)
{
    ui->setupUi(this);
    this->storage_=storage;
}

login::~login()
{
    delete ui;

}
int login::getUserId()
{
    return user_id;
}
QString hashPassword(QString const & password)
{

    QByteArray pass_ba = password.toUtf8();
    QByteArray hash_ba = QCryptographicHash::hash(pass_ba, QCryptographicHash::Md5);
    QString pass_hash = QString(hash_ba.toHex());
    return pass_hash;

}
void login::on_oklog_clicked()
{
    string username = ui->login2->text().toStdString();
    string password = ui->password->text().toStdString();
    string hashed_pass = hashPassword(QString::fromStdString(password)).toStdString();
    experimental::optional<Users> tmp = storage_->getUserAuth(username, hashed_pass);
    if (tmp)
    {
        Users user = tmp.value();
        user_id = user.id;
        QMessageBox::information(this, "Success", "User confirmed.");
        this->close();
    }
    else
    {
        QMessageBox::information(this, "Error", "No such users found, please, try again.");
    }
}

void login::on_nolog_clicked()
{
    QMessageBox::information(this, "User unconfirmed.", "Try to open storage again");
    this->close();
}
