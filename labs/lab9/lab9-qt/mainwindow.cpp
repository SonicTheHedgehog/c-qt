#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    visibility();
    setWindowTitle(tr("Information about social media"));
    connect(ui->actionOpen_Storage, &QAction::triggered, this, &MainWindow::OpenStorage);
    connect(ui->actionExit, &QAction::triggered, this, &MainWindow::Exit);
    connect(ui->actionlogout, &QAction::triggered, this, &MainWindow::Logout);
}

MainWindow::~MainWindow()
{
    storage_->close();
    delete storage_;
    delete ui;
}
void MainWindow::visibility()
{
    bool v;
    int count = ui->listWidget->selectedItems().count();
    if(count != 0) {v = true;}
    else {v = false;}
    ui->widget->setVisible(v);
}
void MainWindow::media_details(const Social & media) {

    ui->lid->setText(QString::number(media.id));
    ui->lname->setText(QString::fromStdString(media.name));
    ui->lusers->setText(QString::number(media.users));
    ui->lrevenue->setText(QString::number(media.revenue));
    ui->lactive->setText(QString::number(media.active));

}
void MainWindow::on_listWidget_itemClicked(QListWidgetItem *item) {

    visibility();
    ui->Edit->setEnabled(true);
    ui->remove->setEnabled(true);
    QVariant var = item->data(Qt::UserRole);
    Social tmp = var.value<Social>();
    media_details(tmp);
}

void MainWindow::OpenStorage()
{
    if (storage_ != nullptr)
    {
        delete storage_;
    }
    QFileDialog dialog(this);
    dialog.setFileMode(QFileDialog::Directory);
    QString folder_path = dialog.getExistingDirectory(this, "Select Folder");
    if(folder_path.isEmpty())
    {
        return;
    }
    storage_=new SqliteStorage{folder_path.toStdString()};
    storage_->open();
    login log(this, storage_);
    log.setWindowTitle("Authentication");
    log.exec();
    user_id = log.getUserId();
    vector<Social> media=storage_->getAllUserSocial(user_id);
    ui->listWidget->clear();
    visibility();
    ui->Add->setEnabled(true);
    ui->actionlogout->setEnabled(true);
    for(auto it=media.begin(); it!=media.end(); it++)
    {
        QListWidget * listWidget=ui->listWidget;
        QVariant var = QVariant::fromValue((*it));
        QListWidgetItem * new_item = new QListWidgetItem();
        new_item->setText(QString::fromStdString(it->name));
        new_item->setData(Qt::UserRole, var);
        listWidget->addItem(new_item);
    }

}
void MainWindow::Logout()
{
    user_id = 0;
    ui->listWidget->clear();
    storage_->close();
    delete storage_;
    storage_=nullptr;
    ui->actionOpen_Storage->setEnabled(true);
    ui->actionlogout->setEnabled(false);
    visibility();
}
void MainWindow::on_Add_clicked()
{
    if (storage_ != nullptr)
    {
        Dialog addDialog(this);
        addDialog.setWindowTitle("Adding");
        int status = addDialog.exec();
        if (status == 1)
        {
            Social media = addDialog.MainSMedia();
            int id= storage_->insertMedia(media);
            bool t=storage_->addUserid(user_id, id);
            media.id = id;
            QVariant var = QVariant::fromValue(media);
            QListWidgetItem * new_item = new QListWidgetItem();
            new_item->setText(QString::fromStdString(media.name));
            new_item->setData(Qt::UserRole, var);
            ui->listWidget->addItem(new_item);
            QMessageBox::information(this, "Inserted", "New media id: " + QString::number(id));
        }
    }
}

void MainWindow::on_Edit_clicked()
{
    QList<QListWidgetItem *> items = ui->listWidget->selectedItems();
    if(items.count() == 1)
    {
        QListWidgetItem * selectedItem = items.at(0);
        QVariant var = selectedItem->data(Qt::UserRole);
        Social tmp = var.value<Social>();
        EditSocial editDialog(this, storage_);
        editDialog.setWindowTitle("Editing");
        editDialog.EditLabels(tmp);
        int status = editDialog.exec();
        if (status == 1)
        {
            Social media = editDialog.MainSMedia();
            media.id = tmp.id;
            bool updated = storage_->updateMedia(media);
            if(updated)
            {
                selectedItem->setText(QString::fromStdString(media.name));
                QVariant tmpvar = QVariant::fromValue(media);
                selectedItem->setData(Qt::UserRole, tmpvar);
                media_details(media);
                QMessageBox::information(this, "Success", "Social media has been updated.");
            }
            else
            {
                QMessageBox::warning(this, "Error", "Can't update this social media.");
            }
        }
    }
    else if(items.count() ==0)
    {
        QMessageBox::warning(
                    this,
                    "Choose an item",
                    "You didnt't choose anything to edit");
        ui->Edit->setEnabled(false);
        ui->remove->setEnabled(false);
    }
    else
    {
        QMessageBox::warning(
                    this,
                    "Choose an item",
                    "You chose more than 1 to edit");
        ui->Edit->setEnabled(false);
        ui->remove->setEnabled(false);
    }
}

void MainWindow::on_remove_clicked()
{
    QList<QListWidgetItem *> items = ui->listWidget->selectedItems();
    if (items.count() > 0)
    {
        QMessageBox::StandardButton reply;
        reply = QMessageBox::question(
                    this,
                    "On remove",
                    "Are you sure?");
        if (reply == QMessageBox::StandardButton::Yes)
        {
            QListWidgetItem * selectedItem = items.at(0);
            QVariant var = selectedItem->data(Qt::UserRole);
            Social tmp = var.value<Social>();
            int social_id = tmp.id;
            vector<Groups> links = storage_->getAllSocialGroups(social_id);
            bool removed=true;
            for (Groups & group : links)
            {
                removed=storage_->removeSocialGroups(social_id, group.id);
            }
            if(removed)
            {
                foreach (QListWidgetItem * item, items)
                {
                    QVariant var = item->data(Qt::UserRole);
                    Social tmp = var.value<Social>();
                    storage_->removeMedia(tmp.id) ;
                    delete ui->listWidget->takeItem(ui->listWidget->row(item));
                    visibility();
                }
            }
            else
            {
                QMessageBox::warning(
                            this,
                            "Somethig went wrong",
                            "For some reasons removing of social media was interrupted");
            }
        }
    }
    else if(items.count() == 0)
    {
        QMessageBox::warning(
                    this,
                    "Choose an item",
                    "You didnt't choose anything to delete");
        ui->Edit->setEnabled(false);
        ui->remove->setEnabled(false);
    }
}

void MainWindow::Exit()
{
    QMessageBox::StandardButton reply;
    reply = QMessageBox::question(
                this,
                "This program will be closed",
                "Are you sure?");
    if (reply == QMessageBox::StandardButton::Yes)
        exit(1);
}
