#ifndef SQLITE_STORAGE_H
#define SQLITE_STORAGE_H
#pragma once
#include <QtSql>
#include <QSqlDatabase>
#include "storage.h"
using namespace std;
class SqliteStorage: public Storage
{
//protected:
        const string dir_name_;
        QSqlDatabase db_;
public:
        SqliteStorage(const string & dir_name);//: Storage(dir_name) {db_ = QSqlDatabase::addDatabase("QSQLITE");}

        bool isOpen() const;
        bool open();
        void close();


         vector<Social> getAllMedia(void) ;
         experimental::optional<Social> getMediaById(int id) ;
         bool updateMedia(const Social &media) ;
         bool removeMedia(int id) ;
         int insertMedia(const Social &media) ;

          vector<Groups> getAllGroups(void);
            experimental::optional<Groups> getGroupById(int id);
           bool updateGroup(const Groups &group);
           bool removeGroup(int id);
           int insertGroup(const Groups &group);
};

#endif // SQLITE_STORAGE_H
