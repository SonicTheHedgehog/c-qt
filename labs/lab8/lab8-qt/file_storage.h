#pragma once

#include <string>
#include <vector>
#include <fstream>
#include <experimental/optional>
#include "social.h"
#include "grups.h"
#include "storage.h"


using namespace std;
class FileStorage: public Storage
{
protected:
  fstream social_media_file;
  fstream groups_file;

 virtual vector<Social> loadMedia()=0;
  virtual void saveMedia(const vector<Social> &media)=0;
 virtual int getNewMediaId()=0;

 virtual vector<Groups> loadGroup()=0;
 virtual void saveGroup(const vector<Groups> &group)=0;
 virtual int getNewGroupId()=0;

public:
  explicit FileStorage(const string &dir_name = ""):Storage(dir_name){}


  bool isOpen() const;
  bool open();
  void close();

  vector<Social> getAllMedia();
  experimental::optional<Social> getMediaById(int id);
  bool updateMedia(const Social &media);
  bool removeMedia(int id);
  int insertMedia(const Social &media);

  vector<Groups> getAllGroups();
  experimental::optional<Groups> getGroupById(int id);
  bool updateGroup(const Groups &group);
  bool removeGroup(int id);
  int insertGroup(const Groups &group);
};

