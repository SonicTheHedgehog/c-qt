#include "storage.h"

Storage::Storage(const string &dir_name) {

    setName(dir_name);

}

void Storage::setName(const string &dir_name) {

    dir_name_ = dir_name;

}

string Storage::name() const {

    return dir_name_;

}
