#include "cui.h"
#include <algorithm>
#include <iostream>
#include <sstream>
#include <QDebug>
using namespace std;
bool isNumber(string str)
{
    if (str.empty())
        return false;
    for (int i = 0; i < str.length(); i++)
    {
        if(str.at(i)=='.')
        {}
        else if (!isdigit(str.at(i)))
            return false;
    }
    return true;
}
void Cui::show()
{
    storage_->open();
    while (1)
    {
        cout << "Choose option:\n1. Main menu\n2. Extra menu\n3. Exit " << endl;
        string choise;
        cin >> choise;
        if (isNumber(choise))
        {
            if (stoi(choise) == 1)
            {
                MainMenu();
            }
            else if(stoi(choise) == 2)
            {
                ExtraMenu();
            }
            else if (stoi(choise) == 3)
            {
                system("clear");
                exit(1);
            }
            else
            {
                cerr << "Wrong choise. Choose 1 or 2" << endl;
                show();
            }
        }
        else
        {
            cerr << "Wrong input. Try input just digits" << endl;
            show();
        }
    }
    storage_->close();

}
void Cui::MainMenu()
{
    cout << "Choose option:\n1. Create\n2. Delete\n3. Get by id\n4. Print\n5. Edit socil media\n6. Back" << endl;
        int ch = 0;
        cin >> ch;
        system("clear");
        switch (ch)
        {
        case 1:
        {
            this->SocialCreateMenu();
            MainMenu();
            break;
        }
        case 2:
        {
            string ids="";
            qDebug()<<"Enter the id of element u want to delete";
            cin>>ids;
            if (isNumber(ids))
            {
                int id=stoi(ids);
                this->SocialDeleteMenu(id);

            }
            else
            {
                qDebug() << "Incorrect input of id.";
            }
            MainMenu();
            break;

        }
        case 3:
        {
            string ids="";
            qDebug()<<"Enter the id of element u want to choose";
            cin>>ids;
            if (isNumber(ids))
            {
                int id=stoi(ids);
                SocialMenu(id);
            }
            else
            {
                qDebug() << "Incorrect input of id.";
            }
            MainMenu();
            break;
        }
        case 4:
        {
            vector<Social> smedia = storage_->getAllMedia();
            for (Social &media : smedia)
            {
                cout << "id: " << media.id << "\tname: " << media.name << endl;
            }
            MainMenu();
            break;
        }
        case 5:
        {
            string ids="";
            qDebug()<<"Enter the id of element u want to change";
            cin>>ids;
            if (isNumber(ids))
            {
                int id=stoi(ids);
                SocialUpdateMenu(id);
            }
            else
            {
                qDebug() << "Incorrect input of id.";
            }
            MainMenu();
            break;
        }
        case 6:
        {
            system("clear");
            show();
            break;
        }
        default:
            cout << "Wrong choise, try again" << endl;
            MainMenu();
            break;
        }
}
void Cui::SocialUpdateMenu(int id)
{
    qDebug()<<"Change social media:";
    Social newsm;
       string name, revenue, active, users;
       cout << "Enter new data\nName: " << endl;
        cin.ignore();
        getline(cin, name);
       cout << "Users:\n";
       cin >> users;
       cout << "Revenue:\n";
       cin >> revenue;
       cout << "Active users:\n";
       cin >> active;
       newsm.name = name;
       if (isNumber(users)&& isNumber(revenue) && isNumber(active))
       {
           newsm.users = stod(users);
           newsm.revenue = stod(revenue);
           newsm.active = stod(active);
           newsm.id = id;
           bool t= storage_->updateMedia(newsm);
           system("clear");
           if(t)
           {qDebug() << "Social network changed!";}
           else
           {qDebug() << "Social network hasn't changed!";}
       }
       else
       {
           qDebug() << "Incorrect input of users, revenue or active ussers. Try again.";
       }
}
void Cui::SocialCreateMenu()
{
    qDebug()<<"Create new social media:";
    Social newsm;
       string name, revenue, active, users;
       cout << "Enter new data\nName: " << endl;
        cin.ignore();
        getline(cin, name);
       cout << "Users:\n";
       cin >> users;
       cout << "Revenue:\n";
       cin >> revenue;
       cout << "Active users:\n";
       cin >> active;
       newsm.name = name;
       if (isNumber(users)&& isNumber(revenue) && isNumber(active))
       {
           newsm.users = stod(users);
           newsm.revenue = stod(revenue);
           newsm.active = stod(active);
           newsm.id = 0;
           int newid = storage_->insertMedia(newsm);
           newsm.id = newid;
           system("clear");
           qDebug() << "New social network added";
       }
       else
       {
           qDebug() << "Incorrect input of users, revenue or active ussers. Try again.";
       }
}
void Cui::SocialDeleteMenu(int id)
{
    if (storage_->removeMedia(id))
    {
        cout << "Removed" << endl;
    }
    else
    {
        cout << "Not removed" << endl;
    }
}
void Cui::SocialMenu(int id)
{
    experimental::optional<Social> opt= storage_->getMediaById(id);
    if(!opt)
    {
        qDebug()<<"Not found";
    }
    else
    {
        Social s=opt.value();
       cout << "id " << s.id << "\tName " << s.name << "\tUsers " << s.users << "\tRevenue " << s.revenue << "\tActive users " << s.active << endl;


    }
}
void Cui::ExtraMenu()
{
    cout << "Choose option:\n1. Create\n2. Delete\n3. Get by id\n4. Print\n5. Edit group\n6.Back" << endl;
    int ch = 0;
    cin >> ch;
    system("clear");
    switch (ch)
    {
    case 1:
    {
        this->GroupCreateMenu();
        ExtraMenu();
        break;
    }
    case 2:
    {
        string ids="";
        qDebug()<<"Enter the id of element u want to delete";
        cin>>ids;
        if (isNumber(ids))
        {
            int id=stoi(ids);
            this->GroupDeleteMenu(id);

        }
        else
        {
            qDebug() << "Incorrect input of id.";
        }
        ExtraMenu();
        break;

    }
    case 3:
    {
        string ids="";
        qDebug()<<"Enter the id of element u want to choose";
        cin>>ids;
        if (isNumber(ids))
        {
            int id=stoi(ids);
            GroupMenu(id);
        }
        else
        {
            qDebug() << "Incorrect input of id.";
        }
        ExtraMenu();
        break;
    }
    case 4:
    {
        vector<Groups> groups = storage_->getAllGroups();
        for (Groups &group : groups)
        {
            cout << "id: " << group.id << "\tname: " << group.name << endl;
        }
        ExtraMenu();
        break;
    }
    case 5:
    {
        string ids="";
        qDebug()<<"Enter the id of element u want to change";
        cin>>ids;
        if (isNumber(ids))
        {
            int id=stoi(ids);
            GroupUpdateMenu(id);
        }
        else
        {
            qDebug() << "Incorrect input of id.";
        }
        ExtraMenu();
        break;
    }
    case 6:
    {
        system("clear");
        show();
        break;
    }
    default:
        cout << "Wrong choise, try again" << endl;
        ExtraMenu();
        break;
    }
 }
void Cui::GroupMenu(int id)
{
    experimental::optional<Groups> opt= storage_->getGroupById( id);
    if(!opt)
    {
        qDebug()<<"Not found";
    }
    else
    {
        Groups s=opt.value();
       cout << "id " << s.id << "\tName " << s.name << "\tUsers " << s.users << "\tAdmin " << s.admin << endl;

    }
}
void Cui::GroupUpdateMenu(int id)
{
    qDebug()<<"Edit group:";
    Groups newgr;
       string name, admin, users;
       cout << "Enter new data\nName: " << endl;
        cin.ignore();
        getline(cin, name);
       cout << "Users:\n";
       cin >> users;
       cout << "Admin:\n";
       cin.ignore();
       getline(cin, admin);
       newgr.name = name;
       newgr.admin=admin;
       if (isNumber(users))
       {
           newgr.users = stoi(users);
           newgr.id = id;
           bool t = storage_->updateGroup(newgr);
           system("clear");
           if(t)
           {
           qDebug() << "Group has been edited";
           }
           else
           {
               qDebug() << "Group hasn't been edited";
           }
       }
       else
       {
           qDebug() << "Incorrect input of users. Try again.";
       }
}
void Cui::GroupCreateMenu()
{
    qDebug()<<"Create new group:";
    Groups newgr;
       string name, admin, users;
       cout << "Enter new data\nName: " << endl;
        cin.ignore();
        getline(cin, name);
       cout << "Users:\n";
       cin >> users;
       cout << "Admin:\n";
       cin.ignore();
       getline(cin, admin);
       newgr.name = name;
       newgr.admin=admin;
       if (isNumber(users))
       {
           newgr.users = stoi(users);
           newgr.id = 0;
           int newid = storage_->insertGroup(newgr);
           newgr.id = newid;
           system("clear");
           qDebug() << "New social network added";
       }
       else
       {
           qDebug() << "Incorrect input of users. Try again.";
       }
}
void Cui::GroupDeleteMenu(int id)
{
    if (storage_->removeGroup(id))
    {
        cout << "Removed" << endl;
    }
    else
    {
        cout << "Not removed" << endl;
    }
}
