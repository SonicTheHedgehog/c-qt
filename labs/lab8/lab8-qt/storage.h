#ifndef STORAGE_H
#define STORAGE_H

#pragma once

#include <string>
#include <vector>
#include <experimental/optional>

#include "social.h"
#include "grups.h"

using namespace std;
class Storage
{
 private:
   string dir_name_;

 public:
   explicit Storage(const string & dir_name="");
   virtual ~Storage() {}

   void setName(const string & dir_name);
   string name() const;

   virtual bool isOpen() const = 0;
   virtual bool open() = 0;
   virtual void close() = 0;

   virtual vector<Social> getAllMedia(void) = 0;
   virtual experimental::optional<Social> getMediaById(int id) = 0;
   virtual bool updateMedia(const Social &media) = 0;
   virtual bool removeMedia(int id) = 0;
   virtual int insertMedia(const Social &media) = 0;


   virtual vector<Groups> getAllGroups(void)= 0;
   virtual  experimental::optional<Groups> getGroupById(int id)= 0;
   virtual bool updateGroup(const Groups &group)= 0;
   virtual bool removeGroup(int id)= 0;
   virtual int insertGroup(const Groups &group)= 0;
};
#endif // STORAGE_H
