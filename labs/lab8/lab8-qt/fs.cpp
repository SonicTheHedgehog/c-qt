#include "fs.h"
#include <fstream>
#include <iostream>
using namespace std;
string read_csvfile(const string &filename)
{
    fstream fin;
    fin.open(filename, std::ofstream::in);
    if (!fin)
    {
        cerr<< "Error opening file" << endl;
        exit(1);
    }
    else
    {
        string line, mainline;
        while (!fin.eof())
        {
            getline(fin, line, '\n');
            int r = line.find('\r');
            if (r != -1)
                line.erase(r);
            mainline = mainline + '\n' + line;
        }
        mainline[mainline.length()] = '\0';
        fin.close();
        return mainline;
    }
}
string read_xmlfile(const string &filename)
{
    fstream fin;
    fin.open(filename, std::ofstream::in);
    if (!fin)
    {
        cerr << "Error opening file" << endl;
        exit(1);
    }
    else
    {
        string line, mainline;
        while (!fin.eof())
        {
            getline(fin, line,'\n');
            int r = line.find('\r');
            if (r != -1)
                line.erase(r);
            mainline+=line;
        }
        fin.close();
        return mainline;
    }
}
int write_file(const string &filename, string &line)
{
    ofstream fout;
    fout.open(filename, ios_base::trunc);
    if (!fout)
    {
        cerr << "Error сclosing file" << endl;
        exit(1);
    }
        fout<<line;
    fout.close();
    return 0;
}
