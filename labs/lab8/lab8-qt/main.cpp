#include "csv_storage.h"
#include "xmlstorage.h"
#include "cui.h"
#include <iostream>
#include "storage.h"
#include "sqlite_storage.h"
using namespace std;
int main()
{
    cout<<"Currently you are working with SQL file"<<endl;
      SqliteStorage sql_storage{"./../data/sql"};
      Storage * storage_ptr=&sql_storage;
      Cui cui{storage_ptr};
      cui.show();
}
