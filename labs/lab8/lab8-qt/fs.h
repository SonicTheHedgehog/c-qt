#ifndef FS_H
#define FS_H

#pragma once
#include <string>
using namespace std;
string read_csvfile(const string &filename);
string read_xmlfile(const string &filename);
int write_file(const string &filename, string &line);
#endif // FS_H
