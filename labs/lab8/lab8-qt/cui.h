#ifndef CUI_H
#define CUI_H
#include <QString>
#include "file_storage.h"
#include "storage.h"
#include "sqlite_storage.h"
using namespace std;
class Cui
{
    Storage *const storage_;
    void MainMenu();
    void SocialMenu(int id);
    void SocialCreateMenu();
    void SocialDeleteMenu(int id);
    void SocialUpdateMenu(int id);
    void ExtraMenu();
    void GroupMenu(int id);
    void GroupCreateMenu();
    void GroupDeleteMenu(int id);
    void GroupUpdateMenu(int id);

public:
    explicit Cui(Storage *storage) : storage_{storage} {}
    void show();
};
#endif // CUI_H
