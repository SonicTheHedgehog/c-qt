#include "sqlite_storage.h"
#include <experimental/optional>
SqliteStorage::SqliteStorage(const string & dir_name): Storage(dir_name)
{
    db_ = QSqlDatabase::addDatabase("QSQLITE");
}
bool SqliteStorage::isOpen() const {

    return db_.isOpen();
}

bool SqliteStorage::open(){
    QString path = QString::fromStdString(this->name()) + "/data.sqlite";
    db_.setDatabaseName(path);
    bool connected = db_.open();
    if (!connected)
    {
        return false;
    }
    return true;
}
void SqliteStorage::close()
{
    db_.close();
}
Social getMediafromQuery(const QSqlQuery &query)
{
    Social media;
    media.id=query.value("id").toInt();
    media.name=query.value("name").toString().toStdString();
    media.users=query.value("users").toDouble();
    media.revenue=query.value("revenue").toDouble();
    media.active=query.value("active").toDouble();
    return media;
}
vector<Social> SqliteStorage::getAllMedia(void)
{
    QSqlQuery query("SELECT * FROM social_media");
    vector<Social> socials;
    while (query.next())
    {
       Social s=getMediafromQuery(query);
       socials.push_back(s);
    }
    return socials;
}
int SqliteStorage::insertMedia(const Social &media)
{
    QSqlQuery query;
    query.prepare("INSERT INTO social_media (name, users, revenue, active) VALUES (:name, :users, :revenue,:active)");
    query.bindValue(":name", QString::fromStdString(media.name));
    query.bindValue(":users", media.users);
    query.bindValue(":revenue", media.revenue);
    query.bindValue(":active", media.active);
    qDebug()<<5;
    if (!query.exec())
    {
       qDebug() << "Insert social media query exec error:" << query.lastError();
       return 0;
    }
    QVariant var=query.lastInsertId();
    qDebug()<<var.toInt();
    return var.toInt();
}
bool SqliteStorage::removeMedia(int id)
{
    QSqlQuery query;
    query.prepare("DELETE FROM social_media WHERE id = :id");
    query.bindValue(":id", id);
    if (!query.exec()){
        return false;
    }
    if(query.numRowsAffected()==0)
    {
        return false;
    }
    return true;

}
experimental::optional<Social> SqliteStorage::getMediaById(int mid)
{
    QSqlQuery query;
    query.prepare("SELECT * FROM social_media WHERE id = :id");
    query.bindValue(":id", mid);
    if(!query.exec())
    {
        qDebug()<<"Get social media error"<<query.lastError();
        return experimental::nullopt;
    }
    if(query.next())
    {
        Social s=getMediafromQuery(query);
        return s;
    }
    else
    {
        return experimental::nullopt;
    }
}
bool SqliteStorage::updateMedia(const Social &media)
{
    QSqlQuery query;
    query.prepare("UPDATE social_media SET name = :name, users=:users, revenue=:revenue, active=:active WHERE id = :id");
    query.bindValue(":name", QString::fromStdString(media.name));
    query.bindValue(":users", media.users);
    query.bindValue(":revenue", media.revenue);
    query.bindValue(":active", media.active);
    query.bindValue(":id", media.id);
    if (!query.exec()){
        qDebug() << "update social media query error:" << query.lastError();
        return false;
    }
    else if(query.numRowsAffected()==0)
    {
        return false;
    }
    return true;
}
Groups getGroupfromQuery(const QSqlQuery &query)
{
    Groups gr;
    gr.id=query.value("id").toInt();
    gr.name=query.value("name").toString().toStdString();
    gr.users=query.value("users").toInt();
    gr.admin= query.value("admin").toString().toStdString();
    return gr;
}
vector<Groups> SqliteStorage::getAllGroups(void)
{
    QSqlQuery query("SELECT * FROM groups");
    vector<Groups> gr;
    while (query.next())
    {
       Groups g=getGroupfromQuery(query);
       gr.push_back(g);
    }
    return gr;
}
experimental::optional<Groups> SqliteStorage::getGroupById(int gid)
{
    QSqlQuery query;
    query.prepare("SELECT * FROM groups WHERE id = :id");
    query.bindValue(":id", gid);
    if(!query.exec())
    {
        qDebug()<<"Get group error"<<query.lastError();
        return experimental::nullopt;
    }
    if(query.next())
    {
        Groups g=getGroupfromQuery(query);
        return g;
    }
    else
    {
        return experimental::nullopt;
    }
}
  bool SqliteStorage::updateGroup(const Groups &group)
  {
      QSqlQuery query;
      query.prepare("UPDATE groups SET name = :name, users=:users, admin=:admin WHERE id = :id");
      query.bindValue(":id", group.id);
      query.bindValue(":name", QString::fromStdString(group.name));
      query.bindValue(":users", group.users);
       query.bindValue(":admin", QString::fromStdString(group.admin));

      if (!query.exec()){
          qDebug() << "Update group query error:" << query.lastError();
          return false;
      }
      else if(query.numRowsAffected()==0)
      {
          return false;
      }
      return true;
  }
  bool SqliteStorage::removeGroup(int id)
  {
      QSqlQuery query;
      query.prepare("DELETE FROM groups WHERE id = :id");
      query.bindValue(":id", id);
      if (!query.exec()){
          return false;
      }
      if(query.numRowsAffected()==0)
      {
          return false;
      }
      return true;
  }
  int SqliteStorage::insertGroup(const Groups &group)
  {
      QSqlQuery query;
      query.prepare("INSERT INTO groups (name,users, admin) VALUES ( :name, :users, :admin)");
      query.bindValue(":name", QString::fromStdString(group.name));
      query.bindValue(":users", group.users);
      query.bindValue(":admin", QString::fromStdString(group.admin));
      if (!query.exec())
      {
         qDebug() << "insert group query error:" << query.lastError();
         return 0;
      }
      QVariant var=query.lastInsertId();
      return var.toInt();
  }
