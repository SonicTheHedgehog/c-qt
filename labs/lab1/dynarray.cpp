#include "dynarray.h" 
#include <iostream>
#include <stdlib.h>
DynamicArray::DynamicArray()
{
    capacity_ = 15;
    items_ = new double[15];
}
size_t DynamicArray::size()
{
    return capacity_;
}
double DynamicArray::get(int index)
{
    if (index < 0 || index > capacity_)
        std::cerr << "there is no such index" << std::endl;
    else
        return items_[index];
}
void DynamicArray::set(int index, double value)
{
    if (index < 0 || index > capacity_)
        std::cerr << "there is no such index" << std::endl;
    else
        items_[index] = value;
}
DynamicArray::~DynamicArray( )
{
    delete[] items_;
}
void DynamicArray::resize(size_t newSize)
{
    double *new_items = new double[newSize];
    std::copy(items_, items_ + 2, new_items);
    for (int i = 0; i < capacity_; i++)
    {
        new_items[i] = items_[i];
    }
    delete items_;
    items_ = new_items;
}