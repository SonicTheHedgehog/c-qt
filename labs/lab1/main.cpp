#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include <iostream>
#include "dynarray.h"
#include "list.h"
#include "deque.h"
#include "fs.h"
using namespace std;
void remake_dq(Deque *n1, Deque *n2, List *n);
void make_ls(Deque *n1, Deque *n2, List *n);
int main()
{
    List L;
    read_file(L);
    cout << "List" << endl;
    L.print();
    L.pushnegetiveback();
    cout << "Remade list" << endl;
    L.print();

    Deque D1;
    Deque D2;
    remake_dq(&D1, &D2, &L);
    cout << "First deque" << endl;
    D1.print();
    cout << "Second deque" << endl;
    D2.print();

    List L1;
    make_ls(&D1, &D2, &L1);
    cout << "Final list" << endl;
    L1.print();
}

void remake_dq(Deque *n1, Deque *n2, List *n)
{
    for (int i = 0; i < n->size(); i++)
    {
        if (i % 2 == 0)
        {
            n2->push_back(n->get(i));
        }
        else
            n1->push_front(n->get(i));
    }
}
void make_ls(Deque *n1, Deque *n2, List *n)
{
    while (!n1->empty())
    {
        n->push_back(n1->pop_back());
    }
    while (!n2->empty())
    {
        n->push_back(n2->pop_front());
    }
}