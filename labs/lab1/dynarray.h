#pragma once
#include <stdlib.h>
class DynamicArray
{
    double *items_;
    size_t capacity_;

public:
    DynamicArray();
    ~DynamicArray();

    size_t size();
    void resize(size_t newSize);

    double get(int index);
    void set(int index, double value);
};
