#include "list.h"
#include "dynarray.h"
#include <iostream>
#include <stdlib.h>
List::List()
{
    DynamicArray array_;
    size_ = 0;
}
size_t List::size()
{
    return size_;
}
double List::get(int index)
{
    if (index < size_)
    {
        return array_.get(index);
    }
    else
        std::cerr << "there is no such index" << std::endl;
}
void List::set(int index, double value)
{
    if (index < size_ && index > 0)
    {
        array_.set(index, value);
    }
    else
        std::cerr << "there is no such index" << std::endl;
}
void List::insert(int index, double value)
{
    if (size_ == array_.size())
    {
        array_.resize(array_.size() * 2);
    }
    else if (index < 0 || index > size_)
        std::cerr << "there is no such index" << std::endl;
    else
    {
        size_++;
        for (int i = size_ - 1; i >= index; i--)
        {
            array_.set(i + 1, array_.get(i));
        }
    }
}
void List::remove_at(int index)
{
    if (index < 0 || index > size_)
        std::cerr << "there is no such index" << std::endl;
    else
    {
        for (int i = index; i < size_ - 1; i++)
        {
            array_.set(i, array_.get(i + 1));
        }
        size_--;
    }
}
void List::push_back(double value)
{
    if (size_ == array_.size())
    {
        array_.resize(array_.size() * 2);
    }
    else
    {
        array_.set(size_, value);
        size_++;
    }
}
int List::index_of(double value)
{
    int index = -1;
    for (int i = 0; i < size_ - 1; i++)
    {
        if (array_.get(i) == value)
        {
            index = i;
            break;
        }
    }

    return index;
}
/*void List::remove(double value)
{
    if (index_of(value) == -1)
    {
        std::cerr << "Error" << std::endl;
    }
    else
    {
        for (int i = index_of(value); i < size_ - 1; i++)
        {
            array_.set(i, array_.get(i + 1));
        }
        size_--;
    }
}*/
bool List::contains(double value)
{
    if (index_of(value) == -1)
    {
        return (false);
    }
    else
    {
        return (true);
    }
}
void List::print()
{
    if (size_ == 0)
    {
        std::cerr <<" empty" << std::endl;
        exit(1);
    }
    else
        for (int i = 0; i < size_; i++)
        {
            std::cerr << array_.get(i) << std::endl;
        }
}
bool List::empty()
{
    return (size_ == 0);
}
void List::clear()
{
    size_ = 0;
}
void List::pushnegetiveback()
{
    for (int i = 0; i < size_; i++)
    {
        if (array_.get(i) < 0)
        {
            push_back(array_.get(i));
            remove_at(i);
        }
    }
}
