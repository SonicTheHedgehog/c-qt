#pragma once
#include <stdlib.h>
#include <stdio.h>
#include "dynarray.h"
class Deque
{
    DynamicArray _array_;
    size_t _size_;

public:
    Deque();

    size_t size(); // return number of items

    void push_back(double value);
    double pop_back();
    void push_front(double value);
    double pop_front();
    void print();

    bool empty();
};