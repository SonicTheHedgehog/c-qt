#include "deque.h"
#include "dynarray.h"
#include <iostream>
#include <stdlib.h>
Deque::Deque()
{
    _size_ = 0;
}
size_t Deque::size()
{
    return _size_;
}
void Deque::push_back(double value)
{
    if (_size_ == _array_.size())
    {
        _array_.resize(_size_++);
    }
    _array_.set(_size_, value);
    _size_++;
}
bool Deque::empty()
{
    return (_size_ == 0);
}
double Deque::pop_back()
{
    double tmp = _array_.get(_size_ - 1);
    _size_--;
    return tmp;
}
void Deque::push_front(double value)
{
    if (_size_ == _array_.size())
    {
        _array_.resize(_size_++);
    }
    for (int i = _size_; i > 0; i--)
    {
        _array_.set(i, _array_.get(i - 1));
    }
    _array_.set(0, value);
    _size_++;
}
double Deque::pop_front()
{
    double tmp = _array_.get(0);
    for (int i = 0; i < _size_; i++)
    {
        _array_.set(i, _array_.get(i + 1));
    }
    _size_--;
    return tmp;
}
void Deque::print()
{

    if (_size_ == 0)
    {
        std::cerr << "empty" << std::endl;
        exit(1);
    }
    else
        for (int i = 0; i < _size_; i++)
        {
            std::cerr << _array_.get(i) << std::endl;
        }
}