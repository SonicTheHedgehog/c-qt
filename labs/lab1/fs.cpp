#include <stdio.h>
#include <stdlib.h>
#include "list.h"
#include <iostream>
#include "fs.h"
int read_file(List &Arr)
{
    FILE *fs = fopen("data.txt", "r");
    if (fs == nullptr)
    {
        fprintf(stderr, "Error opening file \n");
        exit(EXIT_FAILURE);
    }
    int i = 0;
    double num = 0;
    while (fscanf(fs, "%lf", &num) > 0)
    {
        Arr.push_back(num);
        i++;
    }
    if (fclose(fs) != 0)
    {
        fprintf(stderr, "Error closing file \n");
        exit(EXIT_FAILURE);
    }
    fclose(fs);
    return i;
}