#include "fs.h"
#include <fstream>
using namespace std;
StringTable read_file(const string &filename)
{
    fstream fin;
    fin.open(filename, std::ofstream::in);
    if (!fin)
    {
        cerr << "Error opening file" << endl;
        exit(1);
    }
    else
    {
        string line, mainline;
        while (!fin.eof())
        {
            getline(fin, line, '\n');
            int r = line.find('\r');
            if (r != -1)
                line.erase(r);
            mainline = mainline + '\n' + line;
        }
        mainline[mainline.length()] = '\0';
        StringTable table = Csv_parse(mainline);
        fin.close();
        return table;
    }
}
int write_file(const string &filename, StringTable &table)
{
    ofstream fout;
    fout.open(filename, ios_base::trunc);
    if (!fout)
    {
        cerr << "Error opening file" << endl;
        exit(1);
    }
    for (int i = 0; i < table.size_rows(); i++)
    {
        for (int j = 0; j < table.size_cols(); j++)
        {
            fout << table.at(i, j) << ",";
        }
        if (i < table.size_rows() - 1)
            fout << endl;
    }
    fout.close();
    return 0;
}