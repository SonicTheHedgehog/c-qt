#pragma once
#include <string>
#include <iostream>
#include <string>
#include <sstream>
class StringTable
{
   std::string *cells_ = nullptr;
   size_t nrows_ = 0;
   size_t ncols_ = 0;

public:
   StringTable(size_t rows, size_t cols);
   ~StringTable();

   size_t size_rows();
   size_t size_cols();
   void print();
   std::string &at(int colIndex, int rowIndex);
};
