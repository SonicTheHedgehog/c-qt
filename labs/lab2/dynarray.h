#pragma once
#include "social.h"
#include <iostream>
class DynamicArray
{
    struct Social *media;
    size_t capacity_;

public:
    DynamicArray();
    ~DynamicArray();

    size_t size();
    void resize();
    struct Social get(int index);
    void set(int index, struct Social value);
};
