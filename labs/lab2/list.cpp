#include "list.h"
using namespace std;
List::List()
{
    DynamicArray array_;
    size_ = 0;
}
size_t List::size()
{
    return size_;
}
struct Social List::get(int index)
{
    if (index < size_ && index >= 0)
    {
        return array_.get(index);
    }
    else
        cerr << "there is no such index" << endl;
}
void List::set(int index, Social value)
{
    if (index < size_ && index > 0)
    {
        array_.set(index, value);
    }
    else
        cerr << "there is no such index" << endl;
}
void List::push_back(Social value)
{
    if (size_ == array_.size())
    {
        array_.resize();
    }
    else
    {
        array_.set(size_, value);
        size_++;
    }
}
int List::index_of(float value)
{
    int index = -1;
    for (int i = 0; i < size_; i++)
    {
        if (array_.get(i).users > value)
        {
            return i;
        }
    }
    return index;
}
void List::remove(float value)
{
    int index = index_of(value);
    while (index != -1)
    {
        for (int i = index; i < size_ - 1; i++)
        {
            array_.set(i, array_.get(i + 1));
        }
        size_--;
        index = index_of(value);
    }
}

void List::print()
{
    for (int i = 0; i < size_; i++)
    {
        cout << "name " << array_.get(i).name << "\tusers " << array_.get(i).users << "\trevenue " << array_.get(i).revenue << "\tactive users " << array_.get(i).active << endl;
    }
}