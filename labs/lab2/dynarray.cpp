#include "dynarray.h"
#include "social.h"
using namespace std;
DynamicArray::DynamicArray()
{
    capacity_ = 15;
    media = new struct Social[capacity_];
}
size_t DynamicArray::size()
{
    return capacity_;
}
struct Social DynamicArray::get(int index)
{
    if (index < 0 || index > capacity_)
        cerr << "there is no such index" << endl;
    else
        return media[index];
}
void DynamicArray::set(int index, Social value)
{
    if (index < 0 || index > capacity_)
        cerr << "Wrong index" << endl;
    else
    {
        media[index].name = value.name;
        media[index].users = value.users;
        media[index].revenue = value.revenue;
        media[index].active = value.active;
    }
}
DynamicArray::~DynamicArray()
{
    delete[] media;
}
void DynamicArray::resize()
{
    size_t newSize = capacity_ * 2;
    struct Social *newitems = new Social[newSize];
    for (int i = 0; i < capacity_; i++)
    {
        newitems[i].name = media[i].name;
        newitems[i].users = media[i].users;
        newitems[i].revenue = media[i].revenue;
        newitems[i].active = media[i].active;
    }
    delete media;
    media = newitems;
    capacity_ = newSize;
}
