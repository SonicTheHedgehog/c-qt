#pragma once
#include "list.h"
#include "csv.h"
StringTable read_file(const string &filename);
int write_file(const string &filename, StringTable &table);
