#include "list.h"
#include "fs.h"
List createEntityListFromTable(StringTable &csvTable);
StringTable createTableFromEntityList(List &array);
int main()
{
    StringTable inCsvTable = read_file("data.csv");
    cout << "\tString Table" << '\n';
    inCsvTable.print();
    List list = createEntityListFromTable(inCsvTable);
    cout << "\tList" << '\n';
    list.print();
    float N = 0;
    cout << "Enter number of users in billion: ";
    cin >> N;
    list.remove(N);
    cout << "\tNew List" << '\n';
    list.print();
    StringTable outCsvTable = createTableFromEntityList(list);
    cout << "\tNew String Table" << '\n';
    outCsvTable.print();
    write_file("data.csv", outCsvTable);
    return 0;
}
List createEntityListFromTable(StringTable &csvTable)
{
    List list;
    Social media;
    for (int i = 0; i < csvTable.size_rows(); i++)
    {
        media.name = csvTable.at(i, 0);
        media.users = stof(csvTable.at(i, 1));
        media.revenue = csvTable.at(i, 2);
        media.active = csvTable.at(i, 3);
        list.push_back(media);
    }
    return list;
}
StringTable createTableFromEntityList(List &array)
{
    StringTable table(array.size(), 4);
    for (int i = 0; i < table.size_rows(); i++)
    {
        table.at(i, 0) = array.get(i).name;
        ostringstream os;
        os << array.get(i).users;
        table.at(i, 1) = os.str();
        table.at(i, 2) = array.get(i).revenue;
        table.at(i, 3) = array.get(i).active;
    }
    return table;
}