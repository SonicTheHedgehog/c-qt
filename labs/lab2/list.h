#include <string>
#include "dynarray.h"
#pragma once
class List
{
    DynamicArray array_;
    size_t size_;

public:
    List();

    size_t size(); 
    struct Social get(int index);  
    void set(int index,struct Social value);
    void push_back(struct Social value);
    void remove(float value); 
    int index_of(float value); 
    void print();
};