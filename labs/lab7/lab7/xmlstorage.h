#ifndef XMLSTORAGE_H
#define XMLSTORAGE_H
#pragma once
#include "file_storage.h"
using namespace std;

class XmlStorage: public FileStorage
{
    vector<Social> loadMedia();
    void saveMedia(const vector<Social> &media);
    int getNewMediaId();

 public:
   explicit XmlStorage(const string & dir_name_ = "") : FileStorage(dir_name_) {}
};

#endif // XMLSTORAGE_H
