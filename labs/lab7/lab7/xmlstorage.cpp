#include "xmlstorage.h"
#include "fs.h"
#include <QDebug>
#include <QtXml>
#include <QString>
#include <iostream>
#include <QFile>
#include  <QTextStream>
Social xmlElementToSocial(QDomElement & media);
QDomElement socialToXmlElement(QDomDocument & doc, const Social &media);
vector<Social> XmlStorage::loadMedia()
{
    open();
    QString xmlText=QString::fromStdString(read_xmlfile(social_media_file));
    std::vector<Social> media;
        /*while(!social_media_file.atEnd())
        {
            xmlText=social_media_file.readAll();
        }*/
    QDomDocument doc;
    QString errorMessage;
    if (!doc.setContent(xmlText, &errorMessage)) {
        qDebug() << "error parsing xml: " << errorMessage;
        exit(1);
    }
    QDomElement rootEl = doc.documentElement();
    QDomNodeList rootElChildren = rootEl.childNodes();
    for (int i = 0; i < rootElChildren.length(); i++) {
          QDomNode socialNode = rootElChildren.at(i);
          QDomElement socialEl = socialNode.toElement();
          Social sm = xmlElementToSocial(socialEl);
          media.push_back(sm);
    }
    return media;

}
void XmlStorage::saveMedia(const vector<Social> &media)
{
     vector<Social> smedia = media;
     QDomDocument doc;

        QDomElement rootEl = doc.createElement("Social");
        for (const Social & sm: smedia) {
              QDomElement socialEl = socialToXmlElement(doc, sm);
              rootEl.appendChild(socialEl);
        }
        doc.appendChild(rootEl);
        QString qStr = doc.toString(-1);
        string str=qStr.toStdString();
        write_file(this->name() + "/social_media.xml", str);

}
int XmlStorage::getNewMediaId()
{
    vector<Social> smedia = this->loadMedia();
    int max = 0;
    for (Social &sm : smedia)
    {
        if (sm.id > max)
            max = sm.id;
    }
    return max + 1;
}
QDomElement socialToXmlElement(QDomDocument & doc, const Social &media)
{
   QDomElement socialEl = doc.createElement("media");
   socialEl.setAttribute("name", QString::fromStdString(media.name));
   socialEl.setAttribute("users", QString::number(media.users));
   socialEl.setAttribute("revenue", QString::number(media.revenue));
   socialEl.setAttribute("active", QString::number(media.active));
   socialEl.setAttribute("id", media.id);
   return socialEl ;
}
Social xmlElementToSocial(QDomElement & media)
{
   Social social;
   social.name=media.attribute("name").toStdString();
   social.users=media.attribute("users").toDouble();
   social.revenue=media.attribute("revenue").toDouble();
   social.active=media.attribute("active").toDouble();
   social.id=media.attribute("id").toInt();
   return social;
}
