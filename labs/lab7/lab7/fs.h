#ifndef FS_H
#define FS_H

#pragma once
#include <string>
using namespace std;
string read_xmlfile(fstream &fin);
int write_file(const string &filename, string &line);
#endif // FS_H
