#include "fs.h"
#include <fstream>
#include <iostream>
#include <QDebug>
using namespace std;
string read_xmlfile(fstream &fin)
{
    string line, mainline;
    while (!fin.eof())
    {
        getline(fin, line,'\n');
        mainline+=line;
    }
    fin.close();
    return mainline;

}
int write_file(const string &filename, string &line)
{
    ofstream fout;
    fout.open(filename, ios_base::trunc);
    if (!fout)
    {
        cerr << "Error closing file" << endl;
        exit(1);
    }
        fout<<line;
    fout.close();
    return 0;
}
