#ifndef FILE_STORAGE_H
#define FILE_STORAGE_H

#pragma once

#include <string>
#include <vector>
#include <fstream>

#include <experimental/optional>
#include "social.h"


using namespace std;

class FileStorage
{
  string dir_name_;
protected:
  fstream social_media_file;

 virtual vector<Social> loadMedia()=0;
  virtual void saveMedia(const vector<Social> &media)=0;
 virtual int getNewMediaId()=0;


public:
  explicit FileStorage(const string &dir_name = "");
  virtual ~FileStorage() {}

  void setName(const string &dir_name);
  string name() const;
  bool isOpen() const;
  bool open();
  void close();
  vector<Social> getAllMedia();
  experimental::optional<Social> getMediaById(int id);
  bool updateMedia(const Social &media);
  bool removeMedia(int id);
  int insertMedia(const Social &media);

};


#endif // FILE_STORAGE_H
