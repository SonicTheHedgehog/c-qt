#include "file_storage.h"
#include "fs.h"
#include <QDir>
#include<QDebug>
FileStorage::FileStorage(const string &dir_name)
{
    dir_name_ = dir_name;
}
void FileStorage::setName(const string &dir_name)
{
    dir_name_ = dir_name;
}
string FileStorage::name() const
{
    return dir_name_;
}

bool FileStorage::isOpen() const
{
    return (social_media_file.is_open());
}
bool FileStorage::open()
{
    QString dirname = QString::fromStdString(name());
    QDir dir(dirname);
    if (!dir.exists()) {
            dir.mkpath(".");
            vector<Social> empty;
            saveMedia(empty);
        }
    //QString namee=QFileDialog::getOpenFileName(QString::fromStdString(name() + "/social_media.xml"));
    //QFile  social_media_file(namee);
    social_media_file.open(name() + "/social_media.xml");
    return isOpen();
}
void FileStorage::close()
{
    social_media_file.close();
}

vector<Social> FileStorage::getAllMedia()
{
    return this->loadMedia();
}
experimental::optional<Social> FileStorage::getMediaById(int id)
{
    vector<Social> smedia = this->loadMedia();
    for (Social &sm : smedia)
    {
        if (sm.id == id)
        {
            return experimental::optional<Social>{sm};
        }
    }
    return experimental::optional<Social>{};
}
bool FileStorage::updateMedia(const Social &media)
{
    int id = media.id;
    vector<Social> smedia = this->loadMedia();
    for (Social &sm : smedia)
    {
        if (sm.id == id)
        {
            sm = media;
            this->saveMedia(smedia);
            return true;
        }
    }
    return false;
}
bool FileStorage::removeMedia(int id)
{
    vector<Social> smedia = this->loadMedia();
    auto it = smedia.begin();
    auto it_end = smedia.end();
    for (; it != it_end; ++it)
    {
        if ((*it).id == id)
        {
            smedia.erase(it);
            this->saveMedia(smedia);
            return true;
        }
    }
    return false;
}
int FileStorage::insertMedia(const Social &media)
{
    int newID = this->getNewMediaId();
    Social tmp = media;
    tmp.id = newID;
    vector<Social> smedia = this->loadMedia();
    smedia.push_back(tmp);
    this->saveMedia(smedia);
    return newID;
}
