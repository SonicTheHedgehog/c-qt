#ifndef DIALOG_H
#define DIALOG_H
#include "social.h"
#include <QDialog>

namespace Ui {
class Dialog;
}

class Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog(QWidget *parent = 0);
    ~Dialog();

    Social MainSMedia();
    void EditLabels(const Social &media);
private:
    Ui::Dialog *ui;

};

#endif // DIALOG_H
