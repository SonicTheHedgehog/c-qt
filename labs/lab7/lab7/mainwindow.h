#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include <QMainWindow>
#include <QDebug>
#include <QAction>
#include <QFileDialog>
#include <QListWidgetItem>
#include <QMessageBox>
#include <QMainWindow>
#include "file_storage.h"
#include "dialog.h"
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
private slots:
    void OpenStorage();
    void NewStorage();
    void Exit();
    void on_listWidget_itemClicked(QListWidgetItem *item);
    void on_Add_clicked();
    void on_Edit_clicked();
    void on_remove_clicked();

private:
    Ui::MainWindow *ui;
    FileStorage *storage_;
    void visibility();
    void media_details(const Social & media);
};

#endif // MAINWINDOW_H
