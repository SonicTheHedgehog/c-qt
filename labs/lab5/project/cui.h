#include "file_storage.h"
class Cui
{
    FileStorage *const storage_;

    void SMMainMenu();
    void SMExtraMenu();
    void SMMenu(int id);
    void SMEMenu(int id);
    void SMtUpdateMenu(int id);
    void SMtUpdateExtraMenu(int id);
    void SMDeleteMenu(int id);
    void SMDeleteExtraMenu(int id);
    void SMCreateMenu();
    void SMCreateExtraMenu();

public:
    explicit Cui(FileStorage *storage) : storage_{storage} {}
    void show();
};