#pragma once

#include <string>
#include <vector>
#include <fstream>

#include <optional>
#include "social.h"
#include "grups.h"

using namespace std;

class FileStorage
{
  string dir_name_;

  fstream social_media_file;
  fstream groups_file;

  vector<Social> loadMedia();
  void saveMedia(const vector<Social> &media);
  int getNewMediaId();

  vector<Groups> loadGroup();
  void saveGroup(const vector<Groups> &group);
  int getNewGroupId();

public:
  explicit FileStorage(const string &dir_name = "");

  void setName(const string &dir_name);
  string name() const;

  bool isOpen() const;
  bool open();
  void close();

  vector<Social> getAllMedia();
  optional<Social> getMediaById(int id);
  bool updateMedia(const Social &media);
  bool removeMedia(int id);
  int insertMedia(const Social &media);

  vector<Groups> getAllGroups();
  optional<Groups> getGroupById(int id);
  bool updateGroup(const Groups &group);
  bool removeGroup(int id);
  int insertGroup(const Groups &group);
};
