#include "cui.h"
#include <algorithm>
#include <iostream>
using namespace std;
bool isNumber(string str);
void Cui::show()
{
    while (1)
    {
        cout << "Choose option:\n1. Main menu\n2. Extra menu\n3. Exit " << endl;
        string choise;
        cin >> choise;
        if (isNumber(choise))
        {
            if (stoi(choise) == 1)
            {
                SMMainMenu();
            }
            else if(stoi(choise) == 2)
            {
                SMExtraMenu();
            }
            else if (stoi(choise) == 3)
            {
                system("clear");
                exit(1);
            }
            else
            {
                cerr << "Wrong choise. Choose 1 or 2" << endl;
                show();
            }
        }
        else
        {
            cerr << "Wrong input. Try input just digits" << endl;
            show();
        }
    }
}
void Cui::SMMainMenu()
{
    cout << "Choose option:\n1. Print all social network\n2. Choose one by id\n3. Add new one\n4. Back" << endl;
    int ch = 0;
    cin >> ch;
    system("clear");
    switch (ch)
    {
    case 1:
    {
        vector<Social> smedia = storage_->getAllMedia();
        for (Social &media : smedia)
        {
            cout << "id: " << media.id << "\tname: " << media.name << endl;
        }
        SMMainMenu();
        break;
    }
    case 2:
    {
        string id;
        cout << "Enter the id: " << endl;
        cin >> id;
        if (isNumber(id))
        {
            SMMenu(stoi(id));
            SMMainMenu();
            break;
        }
        else
        {
            cerr << "ID should consist of numbers." << endl;
            SMMainMenu();
            break;
        }
    }
    case 3:
    {
        SMCreateMenu();
        SMMainMenu();
        break;
    }
    case 4:
    {
        system("clear");
        show();
        break;
    }
    default:
        cout << "Wrong choise, try again" << endl;
        SMMainMenu();
        break;
    }
}
void Cui::SMMenu(int id)
{
    cout << "Choose option:\n1. Print information\n2. Change fields\n3. Delete this social network\n4. Back" << endl;
    int ch = 0;
    cin >> ch;
    system("clear");
    switch (ch)
    {
    case 1:
    {
        optional<Social> smopt = storage_->getMediaById(id);
        if (smopt)
        {
            Social &sm = smopt.value();
            cout << "id " << sm.id << "\tName " << sm.name << "\tUsers " << sm.users << "\tRevenue " << sm.revenue << "\tActive users " << sm.active << endl;
            SMMenu(id);
            break;
        }
        else
        {
            cout << "There is not such social media" << endl;
            break;
        }
    }
    case 2:
    {
        SMtUpdateMenu(id);
        break;
    }
    case 3:
    {
        SMDeleteMenu(id);
        break;
    }
    case 4:
    {
        system("clear");
        break;
    }
    default:
        cout << "Wrong choise, try again" << endl;
        SMMenu(id);
        break;
    }
}
void Cui::SMCreateMenu()
{
    while (1)
    {
        Social newsm;
        string name, revenue, active, users;
        cout << "Enter new data\nName: " << endl;
        cin >> name;
        cout << "Users:\n";
        cin >> users;
        cout << "Revenue:\n";
        cin >> revenue;
        cout << "Active users:\n";
        cin >> active;
        newsm.name = name;
        newsm.revenue = revenue;
        newsm.active = active;
        if (isNumber(users))
        {
            newsm.users = stof(users);
            newsm.id = 0;
            int newid = storage_->insertMedia(newsm);
            newsm.id = newid;
            system("clear");
            cout << "New social network added" << endl;
            return;
        }
        else
        {
            cerr << "Incorrect input of users. Try again." << endl;
        }
    }
}
void Cui::SMtUpdateMenu(int id)
{
    optional<Social> smopt = storage_->getMediaById(id);
    if (smopt)
    {
        cout << "What exactly do you want to update:\n1. Name\n2. Users\n3. Revenue\n4. Active users\n5. Nothing" << endl;
        int ch = 0;
        cin >> ch;
        Social &sm = smopt.value();
        string name, revenue, active, users;
        switch (ch)
        {
        case 1:
        {
            cout << "Name: " << endl;
            cin >> name;
            sm.name = name;
            if (storage_->updateMedia(sm))
            {
                cout << "Data updated" << endl;
                SMtUpdateMenu(id);
                break;
            }
            else
            {
                cout << "Not updated" << endl;
                SMMenu(id);
                break;
            }
        }
        case 2:
        {
            cout << "Users:\n";
            cin >> users;
            if (isNumber(users))
            {
                sm.users = stof(users);
                if (storage_->updateMedia(sm))
                {
                    cout << "Data updated" << endl;
                    SMtUpdateMenu(id);
                    break;
                }
                else
                {
                    cout << "Not updated" << endl;
                    SMMenu(id);
                    break;
                }
            }
            else
            {
                cerr << "Incorrect input of users. Try again." << endl;
                SMtUpdateMenu(id);
                break;
            }
        }
        case 3:
        {
            cout << "Revenue:\n";
            cin >> revenue;
            sm.revenue = revenue;
            if (storage_->updateMedia(sm))
            {
                cout << "Data updated" << endl;
                SMtUpdateMenu(id);
                break;
            }
            else
            {
                cout << "Not updated" << endl;
                SMMenu(id);
                break;
            }
        }
        case 4:
        {
            cout << "Active users:\n";
            cin >> active;
            sm.active = active;
            if (storage_->updateMedia(sm))
            {
                cout << "Data updated" << endl;
                SMtUpdateMenu(id);
                break;
            }
            else
            {
                cout << "Not updated" << endl;
                SMMenu(id);
                break;
            }
        }
        case 5:
        {
            system("clear");
            SMMenu(id);
            break;
        }
        default:
            system("clear");
            cerr << "Choose one of the options" << endl;
            SMtUpdateMenu(id);
            break;
        }
    }
    else
    {
        cout << "There is not such social media" << endl;
        return;
    }
}
void Cui::SMDeleteMenu(int id)
{
    if (storage_->removeMedia(id))
    {
        cout << "Removed" << endl;
        return;
    }
    else
    {
        cout << "Not removed" << endl;
        SMMenu(id);
        return;
    }
}
bool isNumber(string str)
{
    if (str.empty())
        return false;
    for (int i = 0; i < str.length(); i++)
    {
        if (!isdigit(str.at(i)))
            return false;
    }
    return true;
}
///////////////////////////////////////////
void Cui::SMExtraMenu()
{
    cout << "Choose option:\n1. Print all groups\n2. Choose one by id\n3. Add new one\n4. Back" << endl;
    int ch = 0;
    cin >> ch;
    system("clear");
    switch (ch)
    {
    case 1:
    {
        vector<Groups> grp = storage_->getAllGroups();
        for (Groups &group : grp)
        {
            cout << "id: " << group.id << "\tname: " <<group.name << endl;
        }
        SMExtraMenu();
        break;
    }
    case 2:
    {
        string id;
        cout << "Enter the id: " << endl;
        cin >> id;
        if (isNumber(id))
        {
            SMEMenu(stoi(id));
            SMExtraMenu();
            break;
        }
        else
        {
            cerr << "ID should consist of numbers." << endl;
            SMExtraMenu();
            break;
        }
    }
    case 3:
    {
        SMCreateExtraMenu();
        SMExtraMenu();
        break;
    }
    case 4:
    {
        system("clear");
        show();
        break;
    }
    default:
        cout << "Wrong choise, try again" << endl;
        SMExtraMenu();
        break;
    }
}
void Cui::SMEMenu(int id)
{
    cout << "Choose option:\n1. Print information\n2. Change fields\n3. Delete this group\n4. Back" << endl;
    int ch = 0;
    cin >> ch;
    system("clear");
    switch (ch)
    {
    case 1:
    {
        optional<Groups> gopt = storage_->getGroupById(id);
        if (gopt)
        {
            Groups &grp = gopt.value();
            cout << "id " << grp.id << "\tName " << grp.name << "\tUsers " << grp.users << "\tAdmin" << grp.admin << endl;
            SMEMenu(id);
            break;
        }
        else
        {
            cout << "There is not such group" << endl;
            break;
        }
    }
    case 2:
    {
        SMtUpdateExtraMenu(id);
        break;
    }
    case 3:
    {
        SMDeleteExtraMenu(id);
        break;
    }
    case 4:
    {
        system("clear");
        break;
    }
    default:
        cout << "Wrong choise, try again" << endl;
        SMEMenu(id);
        break;
    }
}
void Cui::SMCreateExtraMenu()
{
    while (1)
    {
        Groups newsm;
        string name, admin, users;
        cout << "Enter new data\nName: " << endl;
        cin >> name;
        cout << "Users:\n";
        cin >> users;
        cout << "Admin:\n";
        cin >> admin;
        newsm.name = name;
        newsm.admin = admin;
        if (isNumber(users))
        {
            newsm.users = stoi(users);
            newsm.id = 0;
            int newid = storage_->insertGroup(newsm);
            newsm.id = newid;
            system("clear");
            cout << "New group added" << endl;
            return;
        }
        else
        {
            cerr << "Incorrect input of users. Try again." << endl;
        }
    }
}
void Cui::SMtUpdateExtraMenu(int id)
{
    optional<Groups> gopt = storage_->getGroupById(id);
    if (gopt)
    {
        cout << "What exactly do you want to update:\n1. Name\n2. Users\n3. Admin\n4. Nothing" << endl;
        int ch = 0;
        cin >> ch;
        Groups &grp = gopt.value();
        string name, admin, users;
        switch (ch)
        {
        case 1:
        {
            cout << "Name: " << endl;
            cin >> name;
            grp.name = name;
            if (storage_->updateGroup(grp))
            {
                cout << "Data updated" << endl;
                SMtUpdateExtraMenu(id);
                break;
            }
            else
            {
                cout << "Not updated" << endl;
                SMEMenu(id);
                break;
            }
        }
        case 2:
        {
            cout << "Users:\n";
            cin >> users;
            if (isNumber(users))
            {
                grp.users = stoi(users);
                if (storage_->updateGroup(grp))
                {
                    cout << "Data updated" << endl;
                    SMtUpdateExtraMenu(id);
                    break;
                }
                else
                {
                    cout << "Not updated" << endl;
                    SMEMenu(id);
                    break;
                }
            }
            else
            {
                cerr << "Incorrect input of users. Try again." << endl;
                SMtUpdateExtraMenu(id);
                break;
            }
        }
        case 3:
        {
            cout << "Admin:\n";
            cin >> admin;
            grp.admin=admin;
            if (storage_->updateGroup(grp))
            {
                cout << "Data updated" << endl;
                SMtUpdateExtraMenu(id);
                break;
            }
            else
            {
                cout << "Not updated" << endl;
                SMEMenu(id);
                break;
            }
        }
        case 4:
        {
            system("clear");
            SMEMenu(id);
            break;
        }
        default:
            system("clear");
            cerr << "Choose one of the options" << endl;
            SMtUpdateExtraMenu(id);
            break;
        }
    }
    else
    {
        cout << "There is not such group" << endl;
        return;
    }
}
void Cui::SMDeleteExtraMenu(int id)
{
    if (storage_->removeGroup(id))
    {
        cout << "Removed" << endl;
        return;
    }
    else
    {
        cout << "Not removed" << endl;
        SMMenu(id);
        return;
    }
}
