#include "file_storage.h"
#include "cui.h"
using namespace std;
int main()
{
    FileStorage fs{"../../data/"};
    fs.open();
    Cui cui{&fs};
    cui.show();
    fs.close();
}
