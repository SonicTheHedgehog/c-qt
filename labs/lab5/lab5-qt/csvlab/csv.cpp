#include "csv.h"

#include "csv.h"
using namespace std;
StringTable Csv_parse(string &csvStr)
{
    int col = 0, row = 0;

    for (int i = 0; csvStr[i] != '\0'; i++)
    {
        if (csvStr[i] == ',')
        {
            col++;
        }
        else if (csvStr[i] == '\n')
        {
            row++;
        }
    }
    col /= row;
    col++;
    StringTable stringtable(row, col);
    string tmp;
    csvStr = csvStr.substr(csvStr.find_first_of('\n') + 1);
    size_t rowpos = 0, colpos = 0;
    for (int i = 0; i < row; i++)
    {
        for (int j = 0; j < col - 1; j++)
        {
            colpos = csvStr.find_first_of(',');
            tmp = csvStr.substr(0, colpos);
            colpos += 1;
            csvStr = csvStr.substr(colpos);
            stringtable.at(i, j) = tmp;
        }
        rowpos = csvStr.find_first_of('\n');
        tmp = csvStr.substr(0, rowpos);
        stringtable.at(i, col - 1) = tmp;
        rowpos += 1;
        csvStr = csvStr.substr(rowpos);
    }
    return stringtable;
}

string Csv_toString(StringTable &table)
{
    string csvStr;
    for (int i = 0; i < table.size_rows(); i++)
    {
        for (int j = 0; j < table.size_cols(); j++)
        {
            csvStr += table.at(i, j);
            if (j < table.size_cols() - 1)
                csvStr += ',';
        }
        if (i < table.size_rows() - 1)
            csvStr += '\n';
    }
    return csvStr;
}
