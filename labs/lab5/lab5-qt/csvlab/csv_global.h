#ifndef CSVLAB_GLOBAL_H
#define CSVLAB_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(CSVLAB_LIBRARY)
#  define CSVLABSHARED_EXPORT Q_DECL_EXPORT
#else
#  define CSVLABSHARED_EXPORT Q_DECL_IMPORT
#endif

#endif // CSVLAB_GLOBAL_H
