#include "file_storage.h"
#include "fs.h"
FileStorage::FileStorage(const string &dir_name)
{
    dir_name_ = dir_name;
}
void FileStorage::setName(const string &dir_name)
{
    dir_name_ = dir_name;
}
string FileStorage::name() const
{
    return dir_name_;
}

bool FileStorage::isOpen() const
{
    return (social_media_file.is_open() && groups_file.is_open());
}
bool FileStorage::open()
{
    social_media_file.open(dir_name_ + "social_media.csv");
    groups_file.open(dir_name_ + "groups.csv");
    return (social_media_file.is_open() && groups_file.is_open());
}
void FileStorage::close()
{
    social_media_file.close();
    groups_file.close();
}

vector<Social> FileStorage::getAllMedia()
{
    return this->loadMedia();
}
experimental::optional<Social> FileStorage::getMediaById(int id)
{
    vector<Social> smedia = this->loadMedia();
    for (Social &sm : smedia)
    {
        if (sm.id == id)
        {
            return experimental::optional<Social>{sm};
        }
    }
    return experimental::optional<Social>{};
}
bool FileStorage::updateMedia(const Social &media)
{
    int id = media.id;
    vector<Social> smedia = this->loadMedia();
    for (Social &sm : smedia)
    {
        if (sm.id == id)
        {
            sm = media;
            this->saveMedia(smedia);
            return true;
        }
    }
    return false;
}
bool FileStorage::removeMedia(int id)
{
    vector<Social> smedia = this->loadMedia();
    auto it = smedia.begin();
    auto it_end = smedia.end();
    for (; it != it_end; ++it)
    {
        if ((*it).id == id)
        {
            smedia.erase(it);
            this->saveMedia(smedia);
            return true;
        }
    }
    return false;
}
int FileStorage::insertMedia(const Social &media)
{
    int newID = this->getNewMediaId();
    Social tmp = media;
    tmp.id = newID;
    vector<Social> smedia = this->loadMedia();
    smedia.push_back(tmp);
    this->saveMedia(smedia);
    return newID;
}

//////////////////////////////////////////////////////////////////
vector<Groups> FileStorage::getAllGroups()
{
    return this->loadGroup();
}
experimental::optional<Groups> FileStorage::getGroupById(int id)
{
    vector<Groups> grp = this->loadGroup();
    for (Groups &ngrp : grp)
    {
        if (ngrp.id == id)
        {
            return experimental::optional<Groups>{ngrp};
        }
    }
    return experimental::optional<Groups>{};
}
bool FileStorage::updateGroup(const Groups &group)
{
    int id = group.id;
    vector<Groups> grp = this->loadGroup();
    for (Groups &ngrp : grp)
    {
        if (ngrp.id == id)
        {
            ngrp = group;
            this->saveGroup(grp);
            return true;
        }
    }
    return false;
}
bool FileStorage::removeGroup(int id)
{
    vector<Groups> grp = this->loadGroup();
    auto it = grp.begin();
    auto it_end = grp.end();
    for (; it != it_end; ++it)
    {
        if ((*it).id == id)
        {
            grp.erase(it);
            this->saveGroup(grp);
            return true;
        }
    }
    return false;
}
int FileStorage::insertGroup(const Groups &group)
{
    int newID = this->getNewGroupId();
    Groups tmp = group;
    tmp.id = newID;
    vector<Groups> grp = this->loadGroup();
    grp.push_back(tmp);
    this->saveGroup(grp);
    return newID;
}
