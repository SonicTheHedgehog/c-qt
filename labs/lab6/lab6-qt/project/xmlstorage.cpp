#include "xmlstorage.h"
#include "fs.h"
#include <QDebug>
#include <QtXml>
#include <QString>
#include <iostream>
Social xmlElementToSocial(QDomElement & media);
QDomElement socialToXmlElement(QDomDocument & doc, const Social &media);
vector<Social> XmlStorage::loadMedia()
{
    vector<Social> smedia;
    string line = read_xmlfile(this->name() + "social_media.xml");
    QString xmlText=QString::fromStdString(line);
    std::vector<Social> media;

       QDomDocument doc;
       QString errorMessage;
       if (!doc.setContent(xmlText, &errorMessage)) {
           qDebug() << "error parsing xml: " << errorMessage;
           exit(1);
       }
       QDomElement rootEl = doc.documentElement();
       QDomNodeList rootElChildren = rootEl.childNodes();
       for (int i = 0; i < rootElChildren.length(); i++) {
             QDomNode socialNode = rootElChildren.at(i);
             QDomElement socialEl = socialNode.toElement();
             Social sm = xmlElementToSocial(socialEl);
             media.push_back(sm);
       }
       return media;

}
void XmlStorage::saveMedia(const vector<Social> &media)
{
     vector<Social> smedia = media;
     QDomDocument doc;

        QDomElement rootEl = doc.createElement("Social");
        for (const Social & sm: smedia) {
              QDomElement socialEl = socialToXmlElement(doc, sm);
              rootEl.appendChild(socialEl);
        }
        doc.appendChild(rootEl);
        QString qStr = doc.toString(-1);
        string str=qStr.toStdString();
        write_file(this->name() + "social_media.xml", str);

}
int XmlStorage::getNewMediaId()
{
    vector<Social> smedia = this->loadMedia();
    int max = 0;
    for (Social &sm : smedia)
    {
        if (sm.id > max)
            max = sm.id;
    }
    return max + 1;
}

QDomElement groupToXmlElement(QDomDocument & doc, const Groups &gr);
Groups xmlElementToGroup(QDomElement & media);
vector<Groups> XmlStorage::loadGroup()
{
    vector<Groups> group;
    string line = read_xmlfile(this->name() + "groups.xml");
    QString xmlText=QString::fromStdString(line);
       QDomDocument doc;
       QString errorMessage;
       if (!doc.setContent(xmlText, &errorMessage)) {
           qDebug() << "error parsing xml: " << errorMessage;
           exit(1);
       }
       QDomElement rootEl = doc.documentElement();
       QDomNodeList rootElChildren = rootEl.childNodes();
       for (int i = 0; i < rootElChildren.length(); i++) {
             QDomNode groupNode = rootElChildren.at(i);
             QDomElement groupEl = groupNode.toElement();
             Groups gr = xmlElementToGroup(groupEl);
             group.push_back(gr);
       }
       return group;
}
void XmlStorage::saveGroup(const vector<Groups> &group)
{
    vector<Groups> gr = group;
    QDomDocument doc;
       QDomElement rootEl = doc.createElement("Groups");
       for (const Groups & ngr: gr) {
             QDomElement groupEl = groupToXmlElement(doc, ngr);
             rootEl.appendChild(groupEl);
       }
       doc.appendChild(rootEl);
       QString qStr = doc.toString(4);
       string str=qStr.toStdString();
       write_file(this->name() + "groups.xml", str);
}
int XmlStorage::getNewGroupId()
{
    vector<Groups> group = this->loadGroup();
    int max = 0;
    for (Groups &gr : group)
    {
        if (gr.id > max)
            max = gr.id;
    }
    return max + 1;
}
QDomElement socialToXmlElement(QDomDocument & doc, const Social &media)
{
   QDomElement socialEl = doc.createElement("media");
   socialEl.setAttribute("name", QString::fromStdString(media.name));
   socialEl.setAttribute("users", media.users);
   socialEl.setAttribute("revenue", QString::fromStdString(media.revenue));
   socialEl.setAttribute("active", QString::fromStdString(media.active));
   socialEl.setAttribute("id", media.id);
   return socialEl ;
}
Social xmlElementToSocial(QDomElement & media)
{
   Social social;
   social.name=media.attribute("name").toStdString();
   social.users=media.attribute("users").toInt();
   social.revenue=media.attribute("revenue").toStdString();
   social.active=media.attribute("active").toStdString();
   social.id=media.attribute("id").toInt();
   return social;
}
QDomElement groupToXmlElement(QDomDocument & doc, const Groups &gr)
{
   QDomElement group = doc.createElement("Group");
   group.setAttribute("id", gr.id);
   group.setAttribute("name", QString::fromStdString(gr.name));
   group.setAttribute("users",gr.users);
   group.setAttribute("admin", QString::fromStdString(gr.admin));
   return group;
}
Groups xmlElementToGroup(QDomElement & media)
{
   Groups gr;
   gr.id=media.attribute("id").toInt();
   gr.name=media.attribute("name").toStdString();
   gr.users=media.attribute("users").toInt();
   gr.admin=media.attribute("admin").toStdString();

   return gr;
}

