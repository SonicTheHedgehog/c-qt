#ifndef CSV_STORAGE_H
#define CSV_STORAGE_H
#pragma once

#include "file_storage.h"

using namespace std;

class CsvStorage: public FileStorage
{
    vector<Social> loadMedia();
    void saveMedia(const vector<Social> &media);
    int getNewMediaId();

    vector<Groups> loadGroup();
    void saveGroup(const vector<Groups> &group);
    int getNewGroupId();

   public:
      explicit CsvStorage(const string & dir_name_ = "") : FileStorage(dir_name_) {}
};

#endif // CSV_STORAGE_H
