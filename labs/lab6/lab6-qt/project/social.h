#ifndef SOCIAL_H
#define SOCIAL_H
#pragma once
#include <string>
using namespace std;
struct Social
{
    string name;
    float users;
    string revenue;
    string active;
    int id;
    bool operator==(Social &other)
    {
        if (users == other.users && name == other.name && revenue == other.revenue && active == other.active && id == other.id)
            return true;
        else
            return false;
    }
     bool operator>(Social &other)
    {
        if (users > other.users && name > other.name && revenue > other.revenue && active > other.active && id > other.id)
            return true;
        else
            return false;
    }
};
#endif // SOCIAL_H
