#include "csv_storage.h"
#include "xmlstorage.h"
#include "cui.h"
#include <iostream>
using namespace std;
int main()
{
    cout<<"With which files would you prefer to work"<<endl;
      cout<<"1.CSV"<<endl;
      cout<<"2.XML"<<endl;
      int ch;
      cin>>ch;
      if(ch==1){
      CsvStorage csvstorage {"../../data/csv/"};
      FileStorage * storage_ptr=&csvstorage;
      Cui cui{storage_ptr};
      cui.show();
      }
      else if(ch==2){
      XmlStorage xml_storage {"../../data/xml/"};
      FileStorage * storage_ptr=&xml_storage;
      Cui cui{storage_ptr};
      cui.show();
      }
      else
      {
          cout<<"Wrong choice. You have no choice anymore."<<endl;
          exit(1);
      }
}
