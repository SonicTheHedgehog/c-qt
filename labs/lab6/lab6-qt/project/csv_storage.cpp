#include "csv_storage.h"
#include "fs.h"
#include "csv.h"
#include <QDebug>
vector<Social> CsvStorage::loadMedia()
{
    vector<Social> smedia;
    qDebug()<<QString::fromStdString(this->name()+  "social_media.csv");
    string csvline = read_csvfile(this->name() + "social_media.csv");
    StringTable csvTable = Csv_parse(csvline);
    Social media;
    if(csvTable.size_cols()==1)
    {
        return smedia;
    }
    else{
    for (int i = 0; i < csvTable.size_rows(); i++)
    {
        media.name = csvTable.at(i, 0);
        media.users = stof(csvTable.at(i, 1));
        media.revenue = csvTable.at(i, 2);
        media.active = csvTable.at(i, 3);
        media.id = stoi(csvTable.at(i, 4));
        smedia.push_back(media);
    }
    this->saveMedia(smedia);
    return smedia;}
}
void CsvStorage::saveMedia(const vector<Social> &media)
{
    vector<Social> smedia = media;
    if(smedia.size()==0)
    {
        ofstream fout;
        fout.open(this->name() + "social_media.csv", ios_base::trunc);
        if (!fout)
        {
            cerr << "Error сlosing file" << endl;
            exit(1);
        }
        fout.close();
    }
    else{
    StringTable table(smedia.size(), 5);
    vector<Social>::iterator sm = smedia.begin();
    for (int i = 0; i < table.size_rows(); i++)
    {
        table.at(i, 0) = sm->name;
        ostringstream os, ss;
        os << sm->users;
        table.at(i, 1) = os.str();
        table.at(i, 2) = sm->revenue;
        table.at(i, 3) = sm->active;
        ss << sm->id;
        table.at(i, 4) = ss.str();
        ++sm;
    }
    string csvline = Csv_toString(table);
    write_file(this->name() + "social_media.csv", csvline);}
}
int CsvStorage::getNewMediaId()
{
    vector<Social> smedia = this->loadMedia();
    int max = 0;
    for (Social &sm : smedia)
    {
        if (sm.id > max)
            max = sm.id;
    }
    return max + 1;
}
vector<Groups> CsvStorage::loadGroup()
{
    vector<Groups> grp;
    string csvline = read_csvfile(this->name() + "groups.csv");
    StringTable csvTable = Csv_parse(csvline);
    Groups group;
    if(csvTable.size_cols()==1)
    {
        return grp;
    }
    else{
    for (int i = 0; i < csvTable.size_rows(); i++)
    {
        group.id = stoi(csvTable.at(i, 0));
        group.name = csvTable.at(i, 1);
        group.users = stoi(csvTable.at(i, 2));
        group.admin = csvTable.at(i, 3);
        grp.push_back(group);
    }
    this->saveGroup(grp);
    return grp;}
}
void CsvStorage::saveGroup(const vector<Groups> &group)
{
    vector<Groups> grp = group;
    StringTable table(grp.size(), 4);
    vector<Groups>::iterator gp = grp.begin();
    cout<<"fgfg"<<table.size_cols()<<"zdfa"<<table.size_rows()<<endl;
    for (int i = 0; i < table.size_rows(); i++)
    {
        ostringstream os, ss;
        os << gp->id;
        table.at(i, 0) = os.str();
        table.at(i, 1) = gp->name;
        ss << gp->users;
        table.at(i, 2) = ss.str();
        table.at(i, 3) = gp->admin;
        ++gp;
    }
    string csvline = Csv_toString(table);
    write_file(this->name() + "groups.csv", csvline);
}
int CsvStorage::getNewGroupId()
{
    vector<Groups> grp = this->loadGroup();
    int max = 0;
    for (Groups &ngrp : grp)
    {
        if (ngrp.id > max)
            max = ngrp.id;
    }
    return max + 1;
}
