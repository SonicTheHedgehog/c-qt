#pragma once

#include <string>
#include <vector>
#include <fstream>
#include <iostream>
#include <optional>
#include "social.h"

using namespace std;

class FileStorage
{
   string  dir_name_;
   fstream social_media_file;

   vector<Social> loadMedia();
   void saveMedia(const vector<Social> & media);
   int getNewMediaId();

 public:
   explicit FileStorage(const string & dir_name = "");

   void setName(const string & dir_name);
   string name() const;

   bool isOpen() const;
   bool open(); 
   void close();

   vector<Social> getAllMedia();
   optional<Social> getMediaById(int id);
   bool updateMedia(const Social &media);
   bool removeMedia(int id);
   int insertMedia(const Social &media);
};

