#include "file_storage.h"
class Cui
{
    FileStorage * const storage_;

    // students menus
    void SMMainMenu();
    void SMMenu(int id);
    void SMtUpdateMenu(int id);
    void SMDeleteMenu(int id);
    void SMCreateMenu();
    
public:
    explicit Cui(FileStorage * storage): storage_{storage} {}
    void show();
};