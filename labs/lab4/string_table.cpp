#include "string_table.h"
using namespace std;
StringTable::StringTable(size_t rows, size_t cols)
{
    if (rows > 0 && cols > 0)
    {
        cells_ = new string[rows * cols];
        if (cells_ == nullptr)
        {
            cerr << "Error allocating memory for the table." << endl;
            exit(1);
        }
        else
        {
            nrows_ = rows;
            ncols_ = cols;
        }
    }
    else
    {
        cerr << "Error allocating memory for the table." << endl;
        exit(1);
    }
}

StringTable::~StringTable()
{
    delete[] cells_;
}
size_t StringTable::size_rows()
{
    return nrows_;
}

size_t StringTable::size_cols()
{
    return ncols_;
}
void StringTable::print()
{
    for (int i = 0; i < size_rows(); i++)
    {
        for (int j = 0; j < size_cols(); j++)
        {
            cout << this->at(i, j) << " ";
        }
       cout << endl;
    } 
}
string &StringTable::at(int rowIndex, int colIndex)
{
    if (rowIndex >= size_rows() || colIndex >= size_cols())
    {
        cerr << "Invalid index." << endl;
        exit(1);
    }
    return cells_[rowIndex * size_cols() + colIndex];
}
