#pragma once
#include "csv.h"
#include <string>
using namespace std;
string read_file(const string &filename);
int write_file(const string &filename, string &line);
