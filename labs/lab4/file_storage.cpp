#include "file_storage.h"
#include "fs.h"
FileStorage::FileStorage(const string &dir_name)
{
    dir_name_ = dir_name;
}
void FileStorage::setName(const string &dir_name)
{
    dir_name_ = dir_name;
}
string FileStorage::name() const
{
    return dir_name_;
}

bool FileStorage::isOpen() const
{
    return social_media_file.is_open();
}
bool FileStorage::open()
{
    social_media_file.open(dir_name_ + "social_media.csv");
    return social_media_file.is_open();
}
void FileStorage::close()
{
    social_media_file.close();
}

vector<Social> FileStorage::getAllMedia()
{

    return this->loadMedia();
}
optional<Social> FileStorage::getMediaById(int id)
{
    vector<Social> smedia = this->loadMedia();
    for (Social &sm : smedia)
    {
        if (sm.id == id)
        {
            return optional<Social>{sm};
        }
    }
    return optional<Social>{};
}
bool FileStorage::updateMedia(const Social &media)
{
    int id = media.id;
    vector<Social> smedia = this->loadMedia();
    for (Social &sm : smedia)
    {
        if (sm.id == id)
        {
            sm = media;
            this->saveMedia(smedia);
            return true;
        }
    }
    return false;
}
bool FileStorage::removeMedia(int id)
{
    vector<Social> smedia = this->loadMedia();
    auto it = smedia.begin();
    auto it_end = smedia.end();
    for (; it != it_end; ++it)
    {
        if ((*it).id == id)
        {
            smedia.erase(it);
            this->saveMedia(smedia);
            return true;
        }
    }
    return false;
}
int FileStorage::insertMedia(const Social &media)
{
    int newID = this->getNewMediaId();
    Social tmp = media;
    tmp.id=newID;
    vector<Social> smedia = this->loadMedia();
    smedia.push_back(tmp);
    this->saveMedia(smedia);
    return newID;
}
vector<Social> FileStorage::loadMedia()
{
    vector<Social> smedia;
    string csvline = read_file(dir_name_ + "social_media.csv");
    StringTable csvTable = Csv_parse(csvline);
    Social media;

    for (int i = 0; i < csvTable.size_rows(); i++)
    {
        media.name = csvTable.at(i, 0);
        media.users = stof(csvTable.at(i, 1));
        media.revenue = csvTable.at(i, 2);
        media.active = csvTable.at(i, 3);
        media.id = stoi(csvTable.at(i, 4));
        smedia.push_back(media);
    }
    this->saveMedia(smedia);
        return smedia;
}
void FileStorage::saveMedia(const vector<Social> &media)
{
    vector<Social> smedia = media;
   // optional<Social> all=smedia->getAllMedia();
    StringTable table(smedia.size(), 5);
    vector<Social>::iterator sm = smedia.begin();
    /* Social media;
     int i=0;
    for(int i=0; i<smedia.size(); i++)
    {
        media.at(i)=*sm;
        sm++;
    }*/
    for (int i = 0; i < table.size_rows(); i++)
    {
        table.at(i, 0) = sm->name;
        ostringstream os, ss;
        os << sm->users;
        table.at(i, 1) = os.str();
        table.at(i, 2) = sm->revenue;
        table.at(i, 3) = sm->active;
        ss<<sm->id;
        table.at(i, 4) =ss.str();
        ++sm;
    }
    string csvline = Csv_toString(table);
    write_file(dir_name_ + "social_media.csv", csvline);
}
int FileStorage::getNewMediaId()
{
    vector<Social> smedia = this->loadMedia();
    int max = 0;
    for (Social &sm : smedia)
    {
        if (sm.id > max)
            max = sm.id;
    }
    return max + 1;
}